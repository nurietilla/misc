
# From original file gets data formated like the following
# only for sucessful requests
# timestamp (unix format),date,latency

import csv
import time
import numpy
import scipy
import datetime
from time import mktime

dataFileReader = csv.reader(open('login_statistics_end_Q4.csv','rb'),delimiter=',',quotechar='"')
timeLatencyFile = csv.writer(open('timeLatency.txt','wb'),delimiter=',',quoting=csv.QUOTE_NONE );

for row in dataFileReader:
	latency = int(row[2]);
	if (row[1]=='1' and row[14].isdigit() and len(row[14])>=10 and latency <30000):
	
		timestamp = float(row[14])
	
		# round the timestamp into day timestamp (no hour or minutes) to calculate daily
		# percentiles
		t = datetime.datetime.fromtimestamp(timestamp);

		t = t.date();
		_day = mktime(t.timetuple())
		

		writerow =[];
		writerow.append( timestamp)
		writerow.append(_day)
		writerow.append(row[2])
		timeLatencyFile.writerow(writerow)
		
