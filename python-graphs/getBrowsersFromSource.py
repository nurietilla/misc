
# From original file gets data formated like the following
# only for sucessful requests
# timestamp (unix format),date,useragent, latency

import csv
import time
import numpy
import scipy
import datetime
from time import mktime
from uasparser import UASparser

uaParser = UASparser('/workplace/browserscope')

dataFileReader = csv.reader(open('login_statistics_end_Q4.csv','rb'),delimiter=',',quotechar='"')
timeLatencyFile = csv.writer(open('timeBrowserLatency.txt','wb'),delimiter=',',quotechar='"' );

for row in dataFileReader:
	latency = int(row[2]);
	userAgent = row[9]
	success = row[1]
	timestamp =row[14]
	
	if (success=='1' and row[14].isdigit() and len(row[14])>=10 and latency <30000   ):
	
		timestamp = float(row[14])
	
		# round the timestamp into day timestamp (no hour or minutes) to calculate daily
		# percentiles
		t = datetime.datetime.fromtimestamp(timestamp);

		t = t.date();
		_day = mktime(t.timetuple())
	
		if userAgent!=None:
			try:
				userAgentObj = uaParser.parse(userAgent,entire_url='ua_icon,os_icon');
			except Exception:
				pass
			else:
				writerow =[];
				writerow.append(timestamp)
				writerow.append(_day)
				writerow.append( userAgentObj['ua_name'])
				writerow.append(latency)
				timeLatencyFile.writerow(writerow)
		
