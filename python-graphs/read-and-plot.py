#!/usr/lib/python
import sys
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime


def main():
    filename = sys.argv[1]
    f = open('./'+filename)

    counts = {}

    # data
    # data[project][date] -> pageviews

    for l in f:
        data = l.split()
        date = data[0]
        project = data[1]
        view_count = data[2]

        if project not in counts:
            counts[project] = []
        counts[project].append((date, view_count))


    # let's print just meta and meta.mw

    meta_counts = {}

    for project in counts.keys():
        # discard badly label data

        if "meta" in project and len(project) <20 and len(counts[project]) >5:
            meta_counts[project] = counts[project]

    print meta_counts

    for project in meta_counts:
        # array of tuples (date, view_count)
        project_counts = meta_counts[project]
        project_counts.sort(key=lambda tup: datetime.strptime(tup[0], '%Y-%m'))

        print project_counts
        times, view_counts = zip(*project_counts)

        print times
        numeric_times = [ datetime.strptime(t, '%Y-%m') for t in times ]
        print numeric_times
        xs = matplotlib.dates.date2num(numeric_times)
        hfmt = matplotlib.dates.DateFormatter('%Y-%m')

        fig, ax = plt.subplots()

        # Use plot_date rather than plot when dealing with time data.
        ax.plot(xs, view_counts)

        # Set the major tick formatter to use your date formatter.
        ax.xaxis.set_major_formatter(hfmt)

        # This simply rotates the x-axis tick labels slightly so they fit nicely.
        fig.autofmt_xdate()
        plt.xlabel(project)
        plt.show()




if __name__=="__main__":
    main()
