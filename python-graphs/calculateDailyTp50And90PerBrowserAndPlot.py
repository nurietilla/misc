
# run getTimestampLatency.py to produce the timeLatency.txt file
#
# reads timestamp,day timestamp, latency file
# works with cleaned files

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint

debug = False;



fileReader = csv.reader(open('timeLatency.txt','rb'),delimiter=',',quoting=csv.QUOTE_NONE)

x = []#timestamp with hour,minutes and seconds
d = []#timestamp with only date information
y =	[]#latencies

july14th = 1310554134
# working with arrays rather than hashmaps as this is what the ploting likes
tpDates = [] #array of days
tp50 =[] #tp50 daily
tp90 =[] #tp90 daily
tpBucket =[];
_day = None
dayTimestamp = None;
_2monthsData = []

for row in fileReader:
	
	timestamp = float(row[0])
	dayTimestamp = float(row[1])
	latency= int(row[2])



	# note that we chnaged rates of 12/28 for now pretend the data set ends
	# note that dataset is not in order
   #	if (timestamp >= 1325052000):
   #		continue;


	# first pass or we moved to the next day
	if(_day == None or _day!=dayTimestamp): 

		#i.e. this is not the 1st pass
		if(_day!=None):
			#calculate percentile using tp bucket
			tpBucketTmp = np.array(tpBucket);
			percentile50 = stats.scoreatpercentile(sorted(tpBucketTmp),50);
			percentile90 = stats.scoreatpercentile(sorted(tpBucketTmp),90);
			tp50.append(percentile50)
			tp90.append(percentile90)

			if(debug):
				print percentile50
				print percentile90
				print len(tpBucket)
				print "----"

		#set new date	
		_day = dayTimestamp
		#clean the tpBucket and start refiling
		tpBucket =[]
		tpDates.append(_day);
		tpBucket.append(latency);

	elif (_day == dayTimestamp):
		# same day, keep appending data
		tpBucket.append(latency);


	# check if timestamp after the boom date, july 14th
	if(timestamp >july14th):
		latency = latency-4000 
		_2monthsData.append(latency)

	x.append(timestamp)
	y.append(latency)
	
# Remove last date, incomplete dataset

del(tpDates[len(tpDates)-2])



#Transform alist into arrays matplotlib uses
y_np = np.array(y)
_2monthsData_np = np.array(_2monthsData);
tp50_np = np.array(tp50);
tp90_np = np.array(tp90);

tpDatesFriendly=[] 


for timest in tpDates:
	friendlyDate = datetime.datetime.fromtimestamp(timest)
	tpDatesFriendly.append(friendlyDate)


tpDates_np = np.array(tpDatesFriendly);


if(debug):
	debugFile = csv.writer(open('debug.txt','wb'),delimiter=',',quoting=csv.QUOTE_NONE );
	i = 0	
	for tpDate in tpDates_np:
		debugrow =[]
		debugrow.append(tpDate)
		debugrow.append(tp50_np[i])
		i = i+1
		debugFile.writerow(debugrow);




#histogram plot with all latency values
f = figure();
f.subplots_adjust(wspace=0.2)
ax1 = subplot(211)
ax1.set_autoscaley_on(False)

plt.xlabel('Page Latency (ms)')
plt.ylabel('Frequency (times)')
plt.title('Page Load Latencies ')

n,bins,patches = plt.hist(y_np,50,normed=0,facecolor='green',alpha=0.75)

plt.grid(True)
plt.xlim([0,20000])
plt.ylim([0,40000])

#TP plot
ax2 = subplot(212)
ax2.set_autoscaley_on(False)
ax2.minorticks_off();
plt.plot(tpDates_np,tp50_np,'b-',label='tp50');
plt.plot(tpDates_np,tp90_np,'r-',label='tp90');
plt.legend(loc='upper left');
plt.ylabel('Page Latency (ms)')
plt.grid(True)
plt.ylim([0,20000])


#blow up of tp50 and tp90
#f=figure()
#ax3 = subplot(211)
#plt.ylabel('Page Latency (ms)')
#plt.title('Page Load Latencies,Detail')
#plt.plot(tpDates_np,tp50_np,'b-',label='tp50');
#plt.plot(tpDates_np,tp90_np,'r-',label='tp90');
#plt.grid(True)
#plt.ylim([0,6000])
#plt.xlim([tpDates_np[100],tpDates_np[170]]);


#ax4 = subplot(212)
#plt.ylabel('Page Latency (ms)')
#plt.plot(tpDates_np,tp50_np,'b-',label='tp50');
#plt.plot(tpDates_np,tp90_np,'r-',label='tp90');
#plt.grid(True)
#plt.ylim([0,8000])
#plt.xlim([tpDates_np[250],tpDates_np[300]]);


# the last two months
#f = figure()
#f.subplots_adjust(wspace=0.2)

#ax5 = subplot(211)

#plt.ylabel('Page Latency (ms)')
#plt.title('Page Load Latencies,Detail')
#plt.plot(tpDates_np,tp50_np,'b-',label='tp50');
#plt.plot(tpDates_np,tp90_np,'r-',label='tp90');

#plt.legend(loc='upper left');
#plt.grid(True)
#plt.ylim([2000,20000])
#plt.xlim([tpDates_np[300],tpDates_np[375]]);




# histogram of latencies last two months
# get from day 300 to day 375 which is what we have on the
# plot above

#ax6 = subplot(212)
#ax6.set_autoscaley_on(False)

#plt.xlabel('Page Latency (ms)')
#plt.ylabel('Frequency (times)')
#plt.title('Page Load Latencies,last two months')

#n,bins,patches = plt.hist(_2monthsData_np,50,normed=0,facecolor='green',alpha=0.75)

#plt.grid(True)
#plt.xlim([0,20000])
#plt.ylim([0,12000])

plt.show()

