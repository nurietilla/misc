#!/usr/local/bin/python

import numpy as np
import matplotlib.pyplot as pyplot
import matplotlib.mlab as mlab
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint


'''file is
1
1
1
2
3
56
34

1 -> 1 user with p(1)
1 -> 1 user with P(1)


34 -> 34 user agents that can be identified with prob 1/34
'''

f = open('./UAProbabilitiesRaw.txt');
lines = f.readlines();
f.close();


prob = {}
for l in lines:
    items = l.split();
    cardinal = float(items[0]);
    probability = round(1/cardinal,3)
    if prob.get(probability)!=None:
        prob[probability] = prob[probability] + cardinal;
    else:
        prob[probability] = cardinal;


# now split hashmap into two arrays
probValues = []
bucketSize = []
percentages = []

# total sample size
total = sum(prob.values())

print prob

for k in prob.keys():
    probValues.append(k);
    bucketSize.append(prob[k]);
    percentages.append(round(prob[k]*100/total,3))


print percentages;

# total sample size



prob_np = np.array(probValues);
bucketSize_np = np.array(bucketSize);



# histogram plot with probability/bucket size
# i think this has to be a bar plot as histogram only plots 1 variable


f = figure();
ax = f.add_subplot(1,1,1)


#ax.xlabel('Probability [0-1]')
#ax.ylabel('Identifiable subjects')
ax.set_title('Uniqness of Raw User Agent Set ')

#bins = prob

#number of bars
n = len(prob_np);


#pyplot.bar(prob_np, percentages,log=1,color='#777777',width=0.10,align='center')
#pyplot.plot(prob_np, percentages, linestyle='', marker='o',alpha=0.50)
ax.scatter(prob_np, percentages, alpha=0.5, edgecolor='black', facecolor='blue',linewidth=0.8,s=50)

pyplot.grid(True)

# calculate y ticks
yticks = []
_maxY = max(bucketSize_np);

i  = 0
while i< _maxY:
    yticks.append(i)
    i = i+5000

#ax.yticks(yticks)
#ax.set_yticks(yticks)

# calculate x ticks
# max is a given ias max prob 1
xticks = []
i  = 0
while i< 1:
    xticks.append(i)
    i = i+0.10


#ax.xticks(xticks)
ax.set_ylabel('Percentage of UA that can be identified (log scale)')
ax.set_xlabel('Probability with which a UA can be identified',fontstyle='italic')
ax.set_xticks(xticks)
ax.set_yscale('log');
ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())


show()
