
# From original file gets data formated like the following
# only for sucessful requests
# timestamp (unix format),date,
import re
import csv
import time
import numpy
import scipy
import datetime
import simplejson
from time import mktime
from uasparser import UASparser

uaParser = UASparser('/workplace/browserscope')

dataFileReader = csv.reader(open('login_statistics_end_Q4.csv','rb'),delimiter=',',quotechar='"',escapechar='\\')
timeLatencyFile = csv.writer(open('timeErrorsBrowser.txt','wb'),delimiter=',',quotechar='"' );

userAgent = None


for row in dataFileReader:
	latency = int(row[2]);
	userAgent = row[9]
	success = row[1]
	timestamp =row[14]
	path = row[3]# not a very intertesting data point as most requests are in m=Home&func=index
	
	if (success=='0'and row[14].isdigit() and len(row[14])>=10 and row[5]!=''):
		badFiles =[]	
		timestamp = float(row[14])

		try:
			badFiles = simplejson.loads(row[5])
		except ValueError:
			next


		# Each file we could not load is keep track of
		numOfFilesInError = len(badFiles)


		# round the timestamp into day timestamp (no hour or minutes) to calculate daily
		# percentiles
		t = datetime.datetime.fromtimestamp(timestamp);

		t = t.date();
		_day = mktime(t.timetuple())

		
		if userAgent!=None:
			try:
				userAgentObj = uaParser.parse(userAgent,entire_url='ua_icon,os_icon');
			except Exception:
				pass
			else:
				writerow =[];
				writerow.append(timestamp)
				writerow.append(_day)
				writerow.append(numOfFilesInError)
				writerow.append( userAgentObj['ua_name'])
				#writerow.append(userAgent)
				timeLatencyFile.writerow(writerow)

				

