
# 
#
# reads timestamp,day timestamp, browser
# works with cleaned files

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint
import re

debug = False
#debug = True


def percentage(total,value):
	value = float(value)
	total = float(total)

	p = (value/total)*100
	return p


# removing outliers using percentiles, works for this dataset
# as it is uniform and has few outliers 
# this method wouldn't work for most datasets
def removeOutliers(data):
	data_sorted = sorted(data)
	tp98data = stats.scoreatpercentile(sorted(data),98);
	tp50data = stats.scoreatpercentile(sorted(data),50)
	tp2data = stats.scoreatpercentile(sorted(data),2)

	i = 0

	if (debug):
		print tp95data
		print tp50data

		print data
		print "-----"

	for item in data:
		if item >tp98data:	
			data[i] = tp98data
		elif item <tp2data:
			data[i] = tp2data
		i= i+1

	return data



fileReader = csv.reader(open('timeBrowserLatency.txt','rb'),delimiter=',',quoting=csv.QUOTE_NONE)

x = []#timestamp with hour,minutes and seconds
d = []#timestamp with only date information

# working with arrays rather than hashmaps as this is what the ploting likes
tpDates = [] #array of days
ff =[] #
ff3 =[]
ff4 =[]
ff5 =[]
ff6 =[]
ff7 =[]
ff8 =[]
ff9 =[]
ff10 =[]

ie =[]
ie10 =[]
ie9 =[] #
ie8 =[]
ie7 =[]
ie6 =[]
chrome=[]
safari = []
other = []

android = []
ipad =[];

dt =[]

_day = None
dayTimestamp = None;


ffBrowser = re.compile('Firefox'); 
ieBrowser = re.compile('IE');
chromeBrowser = re.compile('Chrome');
safariBrowser = re.compile('Safari')
_six = re.compile('IE 6.0')
_seven = re.compile('IE 7.0')
_eight = re.compile('IE 8.0')
_nine = re.compile('IE 9.0')
_ten = re.compile('IE 10');

_ff3 = re.compile('Firefox 3')
_ff4 = re.compile('Firefox 4')
_ff5 = re.compile('Firefox 5')
_ff6 = re.compile('Firefox 6')
_ff7 = re.compile('Firefox 7')
_ff8 = re.compile('Firefox 8')
_ff9 =re.compile('Firefox 9')
_ff10 = re.compile('Firefox 10')

_android = re.compile('Android')

_ipad = re.compile('iPad');

ffBucket =0
ff3Bucket =0
ff4Bucket =0
ff5Bucket =0
ff6Bucket =0
ff7Bucket =0
ff8Bucket =0;
ff9Bucket=0;
ff10Bucket=0;

ieBucket =0
ie6Bucket = 0
ie7Bucket = 0
ie8Bucket =0
ie9Bucket =0
ie10Bucket = 0;
ipadBucket =0
chromeBucket =0
safariBucket = 0
androidBucket = 0
otherBucket =0

# we do not add the data for the 1st date in the file
firstDate =  None;

for row in fileReader:
	timestamp = float(row[0])
	dayTimestamp = float(row[1])
	browser = row[2]

	if(firstDate==None):
		firstDate = dayTimestamp


	# note that we changed rates of 12/28 for now pretend the data set ends
	# there
	# if (timestamp >= 1325052000):
	#	continue;


    # first pass or we moved to the next day
	if(_day == None or _day!=dayTimestamp):		

		#i.e. this is not the 1st pass
		if(_day!=None and _day!=firstDate):
			# rather than appending raw values, we calculate
			# a percentage per day per browser
				
			total = ffBucket+ieBucket+chromeBucket+safariBucket+androidBucket+ipadBucket+otherBucket

			ff.append(percentage(total,ffBucket))
			ff3.append(percentage(total,ff3Bucket))
			ff4.append(percentage(total,ff4Bucket))
			ff5.append(percentage(total,ff5Bucket))
			ff6.append(percentage(total,ff6Bucket))
			ff7.append(percentage(total,ff7Bucket))
			ff8.append(percentage(total,ff8Bucket))
			ff9.append(percentage(total,ff9Bucket))
			ff10.append(percentage(total,ff10Bucket))


			ie.append(percentage(total,ieBucket))
			ie6.append(percentage(total,ie6Bucket))

			ie7.append(percentage(total,ie7Bucket))
			ie8.append(percentage(total,ie8Bucket))
			ie9.append(percentage(total,ie9Bucket))
			ie10.append(percentage(total,ie10Bucket));

			chrome.append(percentage(total,chromeBucket))
			safari.append(percentage(total,safariBucket))
			other.append(percentage(total,otherBucket))
			android.append(percentage(total,androidBucket))

			ipad.append(percentage(total,ipadBucket))

			ffBucket =0
			ff3Bucket =0
			ff4Bucket =0
			ff5Bucket =0
			ff6Bucket =0
			ff7Bucket =0
			ff8Bucket=0
			ff9Bucket =0
			ff10Bucket =0

			ieBucket =0
			ie6Bucket =0
			ie7Bucket=0
			ie8Bucket=0
			ie9Bucket=0
			ie10Bucket =10
			ipadBucket =0
			androidBucket =0
			otherBucket =0
			chromeBucket =0
			safariBucket =0
	
		#new date	
		_day = dayTimestamp
		#clean the tpBucket and start refiling
		if(_day!=firstDate):
			dt.append(_day);
		
	
		
#	elif (_day == dayTimestamp):
		 # same day, keep appending  data
		 # something else?
		#print _day

	#in all cases
	#see what browser matches and increment counter

	
	
	if(ffBrowser.match(browser)):
		
		ffBucket = ffBucket+1

		if (_ff3.match(browser)):
			ff3Bucket+=1
		elif (_ff4.match(browser)):
			ff4Bucket+=1
		elif (_ff5.match(browser)):
			ff5Bucket+=1
		elif (_ff6.match(browser)):
			ff6Bucket+=1
		elif (_ff7.match(browser)):
			ff7Bucket+=1
		elif (_ff8.match(browser)):
			ff8Bucket+=1
		elif (_ff9.match(browser)):
			ff9Bucket+=1
		elif (_ff10.match(browser)):
			ff10Bucket+=1

	elif (ieBrowser.match(browser)):
		ieBucket = ieBucket+1
		if (_six.match(browser)):
			ie6Bucket+=1
		elif (_seven.match(browser)):
			ie7Bucket+=1
		elif (_eight.match(browser)):
			ie8Bucket+=1
		elif (_nine.match(browser)):
			ie9Bucket+=1
		elif (_ten.match(browser)):
			ie10Bucket+=1

	elif (chromeBrowser.match(browser)):
		chromeBucket+=1
	elif (_ipad.match(browser)):
		ipadBucket+=1
	elif (safariBrowser.match(browser)):
		safariBucket+=1
	else:
		if(_android.match(browser)):
			androidBucket+=1
		otherBucket+=1



# finished looping through file
# remove last date as for that one we do not have complete data
length =len(dt)
last = length -2
del(dt[last])


#print ie
#print chrome
#print safari
#print other
#print dt

# removing outliers, there are some bad days of data, 3 or 4 in the ff set
# using percentiles to identify outliers and swap them,
# this works for this dataset in question, for most it wouldn't

ff = removeOutliers(ff)
ff3 = removeOutliers(ff3)
chrome = removeOutliers(chrome)
safari = removeOutliers(safari)
ie = removeOutliers(ie)


#Transform a list into arrays matplotlib uses
ff_np = np.array(ff)
ff3_np = np.array(ff3)
ff4_np = np.array(ff4)
ff5_np = np.array(ff5)
ff6_np = np.array(ff6)
ff7_np = np.array(ff7)
ff8_np = np.array(ff8)
ff9_np = np.array(ff9)
ff10_np = np.array(ff10)



ie_np = np.array(ie);
ie6_np = np.array(ie6);
ie7_np = np.array(ie7);
ie8_np = np.array(ie8);
ie9_np = np.array(ie9);
ie10_np =np.array(ie10);

chrome_np = np.array(chrome)
safari_np = np.array(safari)
other_np = np.array(other);
android_np = np.array(android)
ipad_np = np.array(ipad);

tpDatesFriendly=[] 

for timest in dt:
	friendlyDate = datetime.datetime.fromtimestamp(timest)
	tpDatesFriendly.append(friendlyDate)



dt_np = np.array(tpDatesFriendly);


if(debug):
	debugFile = csv.writer(open('debug.txt','wb'),delimiter=',',quoting=csv.QUOTE_NONE );
	i = 0	
	for item in dt_np:
		debugrow =[]
		debugrow.append(ff_np[i])
		debugrow.append(safari_np[i])
		debugrow.append(item)
		i = i+1
		debugFile.writerow(debugrow);


#print ff_np


f = figure();


#X/Y plot
ax2 = subplot(111)
ax2.set_autoscaley_on(False)
ax2.minorticks_off();
plt.plot(dt_np,ff_np,'c-',label='Firefox total');

plt.plot(dt_np,ie_np,'y-',label='IE total');
plt.plot(dt_np,chrome_np,'m-',label='Chrome total');
plt.plot(dt_np,safari_np,'r-',label='Safari total');
plt.plot(dt_np,ipad_np,'r--',label='iPad total');
plt.plot(dt_np,other_np,'g-',label='Other total');
plt.legend(loc='lower left');
plt.ylabel('Percentage of Total Requests')
plt.title('Browser usage at tuenti')
plt.grid(True)
plt.ylim([0,60])


# detail IE 
f= figure()
ax3 = subplot(111)
ax3.set_autoscaley_on(False)
ax3.minorticks_off();
plt.title('Usage of IE')
plt.ylabel('Percentage of Total Requests')
plt.plot(dt_np,ie6_np,'r',label='IE 6');
plt.plot(dt_np,ie7_np,'b',label='IE 7');
plt.plot(dt_np,ie8_np,'c',label='IE 8');
plt.plot(dt_np,ie9_np,'g',label='IE 9');
plt.plot(dt_np,ie10_np,'y',label='IE 10');

plt.legend(loc='upper right');
plt.grid(True)
plt.ylim([0,20])


# detail FF
f= figure()
ax4 = subplot(111)
ax4.set_autoscaley_on(False)
ax4.minorticks_off();
plt.title('Usage of FF')
plt.ylabel('Percentage of Total Requests')
plt.plot(dt_np,ff3_np,'b',label='FF 3.x');
plt.plot(dt_np,ff4_np,'y',label='FF 4');
plt.plot(dt_np,ff5_np,'k',label='FF 5');
plt.plot(dt_np,ff6_np,'g',label='FF 6');
plt.plot(dt_np,ff7_np,'m',label='FF 7');
plt.plot(dt_np,ff8_np,'b--',label='FF 8');
plt.plot(dt_np,ff9_np,'y--',label='FF 9');
plt.plot(dt_np,ff10_np,'k--',label='FF 10');

plt.legend(loc='upper right');
plt.grid(True)
plt.ylim([0,20])



#detail ie & others

f= figure()
ax5 = subplot(111)
ax5.set_autoscaley_on(False)
ax5.minorticks_off();
plt.title('Usage of Minoritary Browsers')
plt.ylabel('Percentage of Total Requests')
plt.plot(dt_np,safari_np,'r',label='Safari');
plt.plot(dt_np,android_np,'g',label='Android Tablet')
plt.plot(dt_np,ipad_np,'r--',label='iPad');
plt.legend(loc='upper right');
plt.grid(True)
plt.ylim([0,3])



plt.show()

