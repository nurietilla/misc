
Handlebars = require('handlebars');
require('./factura.js')
var template = Handlebars.templates["factura.html"];

/**An array containing the command line arguments. 
* The first element will be 'node', the second element will be the name 
* of the JavaScript file. The next elements will be any additional command line arguments.
**/

/**
* This program is executed with the date first (m/d/yy) , amount second 
* and id third
*/


date = process.argv[2]

amount = process.argv[3]

id = process.argv[4]

var templateData = {
    "date": date,
    "amount": amount,
    "id":id,
}


var result = template(templateData);

console.log(result);
