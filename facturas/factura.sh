#!/bin/sh
if [ $# -eq 3 ]; then
    FILE=$3
    `node facturaDriver $1 $2 $3 > $3.html`

    echo "Creating pdf file in current directory: $FILE.pdf"

    `./bin/wkhtmltopdf --encoding 'utf-8'  $3.html $3.pdf`
else
    echo " Missing parameters.Sample usage:"
    echo " factura.sh <date> <amount> <id>"
    echo " factura.sh 1/1/2014 1066.0 1 "
fi
