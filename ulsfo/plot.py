#!/usr/local/bin/python
import datetime as datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.dates as md
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint

# plot percentiles50 and 90 for data for which we have network activity
def main():
    fileReader = csv.reader(open('navigationTimingData26Jan15FebWithConnectStart.txt','rb'),delimiter=',');

    # timestamp
    # country
    # requestStart
    # responseEnd
    # domInteractive
    # mediaWikiLoad

    mediaWikiLoad = list()
    tp50 = list()
    tp90 = list()
    tpBucket =list()
    rs50 = list()
    rs90 = list()
    rsBucket = list()
    dates = list()
    latencies = list()

    # responseStart - connectStart
    tcp50 = list()
    tcp90 = list()
    tcpBucket = list()

    dom50 = list()
    dom90 = list()
    domBucket = list()

    timestamp = None;

    oceania = set(['AS', 'AU', 'CK', 'FJ', 'FM', 'GU', 'KI', 'MH', 'MP' ,'NC','NF','NR','NU','NZ','PF','PG','PN','PW','SB','TK', 'TO', 'TV', 'UM','VU', 'WF'] )
    asia = set(['BD','BT','HK','ID','JP','KH','KP','KR','MM','MN','MO','MY','PH','SG','TH','TW','VN'])
    northAmerica = set(['US','CA'])

    label_oceania = "Ocenania"
    label_northAmerica = "North America";
    label_asia = "SE Asia";

    label = label_asia
    countries = asia

    MIN_BUCKET_SIZE = 1000

    for row in fileReader:
        '''
        result.append(day)
        result.append(country)
        result.append(domInteractive)
        result.append(mediaWikiLoad)
        result.append(dns)
        result.append(event_connectStart)
        result.append(event_connectEnd)
        result.append(requestStart)
        result.append(event_responseStart)
        result.append(responseEnd)
        '''
        recordTimestamp = row[0]
        country = row[1]
        domInteractive = row[2]
        mediaWikiLoadComplete = row[3]
        dns = row[4]
        connectStart = row[5]
        connectEnd = row[6]
        requestStart = row[7]
        responseStart = row[8]
        responseEnd = row[9]

        # using data for which we have dns activity.
        if dns == "NULL"  or country not in countries:
            continue

        networkTime = 0
        if responseStart!="NULL" and connectStart!="NULL":
            networkTime = int(responseStart) - int(connectStart)
            # if responseStart==responseEnd but dnsLookup > 0
            # This indicates a local cache hit
            # dnsLookup is equal to fetchStart when retrieving from local cache.
            # See: https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/NavigationTiming/Overview.html
            responseTime = int(responseStart) - int(responseEnd)
            # # some records report times that seem backwards
            if responseTime == 0 or networkTime > int(mediaWikiLoadComplete):
                continue

        if timestamp != recordTimestamp or timestamp == None:
            # calculate for the day
            if timestamp != None and len(tpBucket) >= MIN_BUCKET_SIZE :
                dates.append(timestamp)
                tp50.append(stats.scoreatpercentile(sorted(tpBucket),50))
                tp90.append(stats.scoreatpercentile(sorted(tpBucket),90))
                rs50.append(stats.scoreatpercentile(sorted(rsBucket),50))
                rs90.append(stats.scoreatpercentile(sorted(rsBucket),90))
                tcp50.append(stats.scoreatpercentile(sorted(tcpBucket),50))
                tcp90.append(stats.scoreatpercentile(sorted(tcpBucket),90))
                dom50.append(stats.scoreatpercentile(sorted(domBucket),50))
                dom90.append(stats.scoreatpercentile(sorted(domBucket),90))

                print timestamp
                print "bucket len"
                print len(tpBucket)

            # reset timestamp
            timestamp = recordTimestamp
            tpBucket = list()
            rsBucket = list()
            tpcBucket = list()
            domBucket = list()

        if requestStart != "NULL":
            rsBucket.append(int(requestStart))

        if networkTime != 0:
            tcpBucket.append(networkTime)

        tpBucket.append(int(mediaWikiLoadComplete))
        latencies.append(int(mediaWikiLoadComplete))
        domBucket.append(int(domInteractive))

    # last record
    if len(tpBucket) >= MIN_BUCKET_SIZE:
        dates.append(recordTimestamp)
        tp50.append(stats.scoreatpercentile(sorted(tpBucket),50))
        tp90.append(stats.scoreatpercentile(sorted(tpBucket),90))
        rs50.append(stats.scoreatpercentile(sorted(rsBucket),50))
        rs90.append(stats.scoreatpercentile(sorted(rsBucket),90))
        tcp50.append(stats.scoreatpercentile(sorted(tcpBucket),50))
        tcp90.append(stats.scoreatpercentile(sorted(tcpBucket),90))
        dom50.append(stats.scoreatpercentile(sorted(domBucket),50))
        dom90.append(stats.scoreatpercentile(sorted(domBucket),90))
        print timestamp
        print "bucket len";
        print len(tpBucket)

    #Transform a list into arrays matplotlib uses
    tp50_np = np.array(tp50)
    tp90_np = np.array(tp90)
    rs50_np = np.array(rs50)
    rs90_np = np.array(rs90)
    tcp50_np = np.array(tcp50)
    tcp90_np = np.array(tcp90)
    dom50_np = np.array(dom50)
    dom90_np = np.array(dom90)

    tpDatesFriendly = []
    tpDates = []

    for d in dates:
        # converts this into matplotlib internal floating point representation
        _date = md.datestr2num(d)
        tpDates.append(_date)
        tpDatesFriendly.append(datetime.datetime.strptime(d, "%Y%m%d%H%M"))

    dates_np = np.array(tpDates);

    # matplotlib date format object
    hfmt = md.DateFormatter('%m/%d')

    f = figure();

    plt.title('Page Load Latencies '+label+'.\n Cold Cache  ')
    plt.plot_date(dates_np, tp50_np,'b-', label='percentile 50');
    plt.plot_date(dates_np, tp90_np, 'r-', label='percentile 90');
    plt.legend(loc='right');
    plt.ylabel('Time (ms)')
    plt.grid(True)
    # bizarre matplotlib date format
    # 735265.  735266.  735267.  735268.  735269.  735270.  735271.  735272
    # Feb 1        2        3        4        5        6        7        8
    ulsfo_time = 735269
    plt.axvline(x=ulsfo_time, linewidth=2, color='m',linestyle='dashed')
    plt.autoscale(True)
    a = gca()
    #a.xaxis.set_major_locator( DayLocator() )
    a.xaxis.set_major_formatter(hfmt)
    #f.autofmt_xdate()
    plt.show()

    plt.title('ResponseStart - ConnectStart. '+label+'.\nNetwork time until first byte minus DNS lookup.')
    plt.plot_date(dates_np, tcp50_np, 'b-', label='percentile 50');
    plt.plot_date(dates_np, tcp90_np, 'r-', label='percentile 90');
    plt.legend(loc='right');
    plt.ylabel('Time (ms)')
    plt.grid(True)
    plt.axvline(x=ulsfo_time, linewidth=2, color='m', linestyle='dashed')
    plt.autoscale(True)
    a = gca()
    a.xaxis.set_major_formatter(hfmt)
    plt.show()

    plt.title('DOMContentInteractive '+label)
    plt.plot_date(dates_np, dom50_np, 'b-', label='percentile 50');
    plt.plot_date(dates_np, dom90_np, 'r-', label='percentile 90');
    plt.legend(loc='right');
    plt.ylabel('Time (ms)')
    plt.grid(True)
    plt.axvline(x=ulsfo_time, linewidth=2, color='m', linestyle='dashed')
    plt.autoscale(True)
    a = gca()
    a.xaxis.set_major_formatter(hfmt)
    plt.show()

if __name__=="__main__":
    main()

