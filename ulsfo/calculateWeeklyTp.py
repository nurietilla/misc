#!/usr/local/bin/python
# calculates per country weekly percentiles
#
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.dates as md
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint
import datetime;


def calculatePercentile(bucket,percentile=50):
    return stats.scoreatpercentile(sorted(bucket),percentile)

# calculate weekly percentiles for the week before, during and after of ulsfo deployment
def main():
    fileReader = csv.reader(open('navigationTimingData26Jan15FebWithConnectStart.txt','rb'),delimiter=',');

    tp50 = {}
    sampleSize ={}
    tpBucket = {}

    # initialize to whatever
    nextWeek = None;

    # intervals
    # 26th Jan to 1st Feb
    # 2nd Feb to 8th Feb
    # 9th Feb to 15th feb
    # week1_time = 201401260000;
    #week2_time = 201402010000;
    #week3_time = 201402080000;
    #week4_time = 201402150000;

    oceania = set(['AS', 'AU', 'CK', 'FJ', 'FM', 'GU', 'KI', 'MH', 'MP' ,'NC','NF','NR','NU','NZ','PF','PG','PN','PW','SB','TK', 'TO', 'TV', 'UM','VU', 'WF'] )
    asia = set(['BD','BT','HK','ID','JP','KH','KP','KR','MM','MN','MO','MY','PH','SG','TH','TW','VN'])
    northAmerica = set(['US','CA'])

    ULSFOCountries = oceania.union(asia,northAmerica)

    # this is a small sample size for 3 days of data
    # 300+ per day
    MIN_BUCKET_SIZE = 1000;

    for row in fileReader:
        '''
        result.append(day)
        result.append(country)
        result.append(domInteractive)
        result.append(mediaWikiLoad)
        result.append(dns)
        result.append(event_connectStart)
        result.append(event_connectEnd)
        result.append(requestStart)
        result.append(event_responseStart)
        result.append(responseEnd)
        '''
        recordTimestamp = datetime.datetime.strptime(row[0], "%Y%m%d%H%M")
        country = row[1]
        domInteractive = row[2]
        mediaWikiLoadComplete = row[3]
        dns = row[4]
        connectStart = row[5]
        connectEnd = row[6]
        requestStart = row[7]
        responseStart = row[8]
        responseEnd = row[9]

        #if country in ULSFOCountries:
         #  continue

        # the very 1st timestamp starts our week count
        if nextWeek == None:
            nextWeek = recordTimestamp + datetime.timedelta(days=6)

        if recordTimestamp > nextWeek:
            print "changing weeks at {0}".format(recordTimestamp)
            # changed weeks
            # calculate tp50 for the week
            # for all countries
            for country in tpBucket.keys():
                if tp50.get(country) == None:
                    tp50[country] = []
                    sampleSize[country] = []
                p = calculatePercentile(tpBucket.get(country))
                tp50.get(country).append(p)
                sampleSize[country].append(len(tpBucket.get(country)))
            # reset
            tpBucket = {}
            nextWeek = recordTimestamp + datetime.timedelta(days=6)

        if tpBucket.get(country)==None:
            tpBucket[country] = []

        tpBucket[country].append(int(mediaWikiLoadComplete))


    # last calculation
    print "changing weeks at {0}".format(recordTimestamp)
    for country in tpBucket.keys():
        p = calculatePercentile(tpBucket.get(country))
        tp50.get(country).append(p)
        sampleSize[country].append(len(tpBucket.get(country)))

    print '{| class="wikitable"'
    print "|-"
    print "!Country !! 50th pctl week1 01/26 (ms)!! 50th pctl week2 02/02 (ms) !! 50th pctl week3 09/02 (ms) !! Difference week1-week2 || Difference week1-week2"
    print "|-"
    for country in tp50.keys():
        tp = tp50[country]
        # we need to have been able to calculate three percentiles
        if len(tp)==3:
            # also weekly sample size per country has to be at least MIN_BUCKET_SIZE
            numberOfSamples = min(sampleSize.get(country));
            if numberOfSamples >= MIN_BUCKET_SIZE and country not in ULSFOCountries:
                msg = "{0}".format(country)
                for t in tp:
                    msg = msg +" || "+str(t)
                difference1 = tp[0] - tp[1]
                difference2 = tp[0] - tp[2]
                print "| {0} || {1} || {2}".format(msg,difference1,difference2)
                print "|-"

    print "|}"

if __name__=="__main__":
    main()

