#!/usr/local/bin/python
# plots mediawikiLoad time for all data points without excluding cached data
#
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.dates as md
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint

def main():
    fileReader = csv.reader(open('navigationTimingData20Jan20FebWithConnectStart.txt','rb'),delimiter=',');

    # timestamp
    # country
    # requestStart
    # responseEnd
    # domInteractive
    # mediaWikiLoad

    mediaWikiLoad = list()
    tp50 = list()
    tp90 = list()
    tpBucket =list()
    rs50 = list()
    rs90 = list()
    rsBucket = list()
    dates = list()
    latencies = list()

    # responseStart - connectStart
    tcp50 = list()
    tcp90 = list()
    tcpBucket = list()

    dom50 = list()
    dom90 = list()
    domBucket = list()

    # initialize to whatever
    timestamp = None;

    oceania = set(['AS', 'AU', 'CK', 'FJ', 'FM', 'GU', 'KI', 'MH', 'MP' ,'NC','NF','NR','NU','NZ','PF','PG','PN','PW','SB','TK', 'TO', 'TV', 'UM','VU', 'WF'] )
    asia = set(['BD','BT','HK','ID','JP','KH','KP','KR','MM','MN','MO','MY','PH','SG','TH','TW','VN'])
    northAmerica = set(['US','CA'])

    countries = oceania

    label = 'Oceania';

    for row in fileReader:
        '''
        result.append(day)
        result.append(country)
        result.append(domInteractive)
        result.append(mediaWikiLoad)
        result.append(dns)
        result.append(event_connectStart)
        result.append(event_connectEnd)
        result.append(requestStart)
        result.append(event_responseStart)
        result.append(responseEnd)
        '''
        recordTimestamp = row[0]
        country = row[1]
        domInteractive = row[2]
        mediaWikiLoadComplete = row[3]
        dns = row[4]
        connectStart = row[5]
        connectEnd = row[6]
        requestStart = row[7]
        responseStart = row[8]
        responseEnd = row[9]

        if country not in countries:
            continue

        if timestamp != recordTimestamp or timestamp == None:
            # calculate tp50 for the day
            if timestamp != None:
                dates.append(timestamp)
                tp50.append(stats.scoreatpercentile(sorted(tpBucket),50))
                tp90.append(stats.scoreatpercentile(sorted(tpBucket),90))

                print "bucket len";
                print len(tpBucket)

            # reset timestamp
            timestamp = recordTimestamp
            tpBucket = list()
            rsBucket = list()
            tpcBucket = list()
            domBucket = list()

        tpBucket.append(int(mediaWikiLoadComplete))
        latencies.append(int(mediaWikiLoadComplete))

    #Transform a list into arrays matplotlib uses
    latencies_np = np.array(latencies)
    tp50_np = np.array(tp50)
    tp90_np = np.array(tp90)

    tpDatesFriendly=[]

    for d in dates:
        # converts this into matplotlib internal floating point representation
        friendlyDate = md.datestr2num(d)
        tpDatesFriendly.append(friendlyDate)

    dates_np = np.array(tpDatesFriendly);

    #histogram plot with all latency values
    f = figure();
    #f.subplots_adjust(wspace=0.2)

    '''
    ax1 = subplot(211)
    ax1.set_autoscaley_on(True)

    plt.xlabel('Page Latency (ms)')
    plt.ylabel('Frequency (times)')
    plt.title('Page Load Latencies OC (Oceania) Countries ')

    n,bins,patches = plt.hist(latencies,50,histtype='bar',facecolor='green',alpha=0.75)

    plt.grid(True)
    plt.xlim([0,15000])
    #plt.ylim([0,400])

    #TP plot
    ax2 = subplot(212)
    ax2.set_autoscaley_on(False)
    ax2.minorticks_off();
    '''
    print dates_np;

    plt.title('Total Page Load Latencies '+label+'.\n Cold and Warm Data ')
    plt.plot_date(dates_np, tp50_np,'b-', label='percentile 50');
    plt.plot_date(dates_np, tp90_np, 'r-', label='percentile 90');

    plt.legend(loc='upper right');
    plt.ylabel('Time (ms)')
    plt.grid(True)
    # 735265.  735266.  735267.  735268.  735269.  735270.  735271.  735272
    # 1        2        3        4        5        6        7        8
    plt.axvline(x=735266, linewidth=2, color='m',linestyle='dashed')
    plt.ylim([1000,6000])

    plt.show()


if __name__=="__main__":
    main()

