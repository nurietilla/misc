#!/usr/local/bin/python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import csv
import datetime
from scipy import stats
from pylab import *
import matplotlib.ticker as ticker
import pprint


'''
"id",0
"uuid",1
"clientIp",2
"clientValidated",3
"isTruncated",4
"timestamp",5
"userAgent",6
"webHost",7
"wiki",8
"event_action",9
"event_connectEnd",10
"event_connectStart",11
"event_dnsLookup",12
"event_domComplete",13
"event_domInteractive",14
"event_fetchStart",15
"event_isAnon",16
"event_isHttps",17
"event_loadEventEnd",18
"event_loadEventStart",19
"event_mediaWikiLoadComplete",20
"event_mobileMode",21
"event_originCountry",22
"event_pageId",23
"event_redirectCount",24
"event_redirecting",25
"event_requestStart",26
"event_responseEnd",27
"event_responseStart",28
"event_revId",29

'''
# Process cvs file and consolidates second timestamps into daily timestamps
def main():

    timing = []

    fileReader = csv.reader(open('navigationTimingData26Jan15FebWithConnectStart.csv','rb'),delimiter=',',quotechar='"');

    for row in fileReader:

        timestamp = row[0]
        requestStart = row[1]
        responseEnd = row[2]
        mediaWikiLoad = row[3]
        domInteractive = row[4]
        country = row[5]
        dns = row[6]
        event_connectStart = row[7]
        event_responseStart = row[8]
        event_connectEnd = row[9]
        # "timestamp","event_requestStart","event_responseEnd","event_mediaWikiLoadComplete","event_domInteractive","event_originCountry","event_dnsLookup","event_connectStart","event_responseStart","event_connectEnd"


        # filter outliers
        if mediaWikiLoad == "NULL" or int(mediaWikiLoad) > 25000 or int(mediaWikiLoad) < 0:
            continue

        result = list()
        day = timestamp[0:8]+'0000'
        result.append(day)
        result.append(country)
        result.append(domInteractive)
        result.append(mediaWikiLoad)
        result.append(dns)
        result.append(event_connectStart)
        result.append(event_connectEnd)
        result.append(requestStart)
        result.append(event_responseStart)
        result.append(responseEnd)

        print "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}".format(*result)

if __name__=="__main__":
    main()
