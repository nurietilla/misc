//serialize/deserialize a tree
//


function Tree (root) {
	this._root = root;

}

Tree.prototype = {
	
	
	getRoot: function() {
		return this._root;
	},

	serialize : function() {
	
		var txt ='';

		var nodes = new Array();

		nodes[0] = this._root;
		while(nodes.length>0) {

			var l = nodes.length;
			var children = new Array();
			for (var i=0;i<l;i++) {
			   node = nodes[i];
			   
			   txt = txt+node.getValue()+",";
				
				if(node.getChildren()) {
					children = children.concat(node.getChildren());
			    }
			}
			//TODO remove last comma
			txt = txt +"|";
			nodes = new Array();
			nodes = children;
		}
		
		return txt;

	},



}


function Node(value){
	this._value = value;
	this._children = null;
}
Node.prototype= {

	getChildren: function() {
		return this._children;
	},

	addChild: function(node) {
	    if(!this._children){ 
			this._children = new Array();
		}
		return this._children.push(node);
	},

	getValue: function() {
		return this._value;
	},

	toString: function() {
		return '{ value='+this._value+'}'
	}



}


var n1 = new Node(1);
var n2 = new Node(2);

var n3 = new Node(3);
var n4 = new Node(4);

n1.addChild(n3);

n3.addChild(n4);
n1.addChild(n2);

var t = new Tree(n1);


console.log(t.serialize());
