function Subscriber(name) {
	this.name = name;
}

Subscriber.prototype.listen = function(publisher) {
	publisher.getSubscribers().push(this);
}


Subscriber.prototype.on = function(_event){
	console.log("Listener: "+this.name+" received  "+_event.name);
}

function Publisher(name) {
	this.name = name;
	this.subscribers = new Array();
	this.getSubscribers = function(){
	    return this.subscribers;
	};

	this.fire = function(_event) {
		var l= this.subscribers.length;
			for (var i=0;i<l;i++) {
	             console.log(this.subscribers[i]);
	             this.subscribers[i].on(_event);
			}
	};

}

/**
Publisher.prototype.getSubscribers = function(){
	return this.subscribers;
}

Publisher.prototype.fire = function(_event) {
		var l= this.subscribers.length;
		for (var i=0;i<l;i++) {
			console.log(this.subscribers[i]);
			this.subscribers[i].on(_event);
		}

}
**/


function CustomEvent(name){
	this.name = name;
}

CustomEvent.prototype = {

	getName: function(){
		return	this.name;
	}
}


function Busy() {
	Publisher.call(this,'juanito');
}



var p = new Publisher('Publisher');

var l1 = new Subscriber('l1 subscriber');

l1.listen(p);

var e = new CustomEvent('juanito');

p.fire(e);

var busy = new Busy();

busy.fire(e);

console.log("busy"+busy);
console.log("Publisher"+p);


