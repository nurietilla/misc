
function mergeSort(_a){

	var len = _a.length;

	if (len<2) {
		return _a;
	} else {
		//divide the problem in two
		//0 is false
		
		var middle = Math.floor(len/2);

		var left = _a.slice(0,middle);

		var right = _a.slice(middle);

		var result = merge(mergeSort(left),mergeSort(right));
		return result;
	}

	
}

/**
* merge two sorted  arrays
* **/
function merge(left, right) {
	var result  = new Array();
	var i=0,j=0;
	var leftLength = left.length;
	var rightLength = right.length;
	var k =0;

	while (i<leftLength && j <rightLength) {
	
		if (left[i] < right[j]){
			
			result[k] = left[i];
			i++;
		} else {
			result[k] = right[j];
			j++;
		}
		k++;

	}
	console.log(left.slice(i));
	console.log(right.slice(j));
	return result.concat(left.slice(i)).concat(right.slice(j));
	
}


var l = [1,6,8,9];
var r=[3];

var a = [6,7,8,2,1,23,37,45,2345,67,87,12,1000,69,3];

console.log(merge(l,r));

console.log("merge sort -----");
console.log(a);

/**var b = mergeSort(a);
console.log(b);**/
