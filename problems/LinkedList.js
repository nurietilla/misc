//reversing a linked list, code problem. doing a js implementation for it.
//
//


function LinkedList() {
	this._length= 0;
	this._head=null;
	this._last=null;
}

//always adds at the end 
LinkedList.prototype = {

	_createNode :function (value) {
		
		function Node() {
			this.data=value;
			this.next=null;
		}
		Node.prototype = {
			toString: function() {
				return "{ data: "+this.data+"}";
			}
		}
		
		return new Node();
	},

	add: function(value) {
		
		var node = this._createNode(value);
        
		if (this._head===null) {
			//first item
			this._head = node;

        } else {
			this._last.next = node;
		}

		//reset last pointer
		this._last = node;
		this._length++;
	},



	reverse :function() {
		//let's do first the obvious of putting it into an array

		var node = this._head;
		var tmpArray = [];
		
		while(node!==null) {
			
			tmpArray.push(node);
			
			node = node.next
		}

		//we do not really need to do this but it is easier to visualize it
		tmpArray.reverse();
		var i = 0;
			
		console.log(tmpArray.toString());

		for(i=0;i<this._length;i++) {
			var	n = tmpArray[i];
			
			if ((i+1)<this._length){
				n.next = tmpArray[i+1];
			} else {
				// last element
				n.next = null;
				this._last = n;
			}
		}
	
		this._head = tmpArray[0];

    },

	reverseInPlace:function(){
			var previous = null;
			var current = this._head;
			var forward = null;

			this._tail = this._head; //mm good that we are in a single threaded env
			
			while(current!== null) {
				forward = current.next;
				current.next = previous;
				previous = current;
				current = forward;
				
			}
			//reset head
			this._head = previous;
			

	},

	toString: function() {
		var node = this._head;
		var txt ="";

		while(node!==null) {
			txt = txt+" "+node.data ;
			node = node.next;
		}
		return txt;
	}

}




var list = new LinkedList();

list.add(7);
list.add(34);
list.add(45);
list.add(67);

var txt = list.toString();

console.log(txt);

//list.reverse();
//var txt = list.toString();
//console.log(txt);


list.reverseInPlace();
console.log(list.toString());
