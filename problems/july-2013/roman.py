#!/usr/local/bin/python
# Implement a function to parse a String representing a Roman numeral.
# example
# I
# II
# III
# IV
# V
# VI
# VII
# VIII
# IX
# X
# L
# XXIV -> 24
# DX   -> 510
# XD   -> 490
# MXV  -> 1015
# the numeral I can be placed before V and X to make 4 units (IV) and 9 units (IX) respectively
# X can be placed before L and C to make 40 (XL) and 90 (XC) respectively
# C can be placed before D and M to make 400 and 900 according to the same
# pattern
import sys;

def parse(romanString):
	# how many letters does the number have
	roman = list(romanString.upper());
	value = 0;
	# value goes on groups of 1 or 2 letters
	# if for each group the letter before is worth less than the one after 
	# we know we are substracting
	
	i = 0;
	l = len(roman);

	while True:
		print str(i) +"   "+ str(value);
		if i >= l-1:
			if i == l-1:
				value = value + getValue(roman[i])
			return value;

		elif isSmaller(roman[i],roman[i+1]):
			value = value + getValue(roman[i+1])-getValue(roman[i]);
			i = i+2;
		else:
			value = value + getValue(roman[i]);
			i = i +1;

	
		

def getValue(letter):
	# TODO check letter, so far happy case assumes it exists 
	return V.get(letter);

def isSmaller(a,b):
	# returns true if a's letter value is smaller than b, false otherwise
	return getValue(a)-getValue(b)< 0

################# program  ########################
V = {};
V["I"] = 1;
V["V"] = 5;
V["X"] = 10;
V["L"] = 50;
V["C"] = 100;
V["D"] = 500;
V["M"] = 1000;
n = sys.argv[1];
print parse(n);
