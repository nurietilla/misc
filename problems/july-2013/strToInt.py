#!/usr/bin/python
########
# convert an integer to a string 
########

import sys
import math

def calculateNumber(i):
	# using two variables in global scope
	# input (input str as an array) and l (length of input)
	if i == 1:
		return matchDigit(input[l-1]);
	else:
	  # extract the number at the given position
	 return matchDigit(input[l-i])*math.pow(10,i-1) +calculateNumber(i-1) 
	 
	
#without using int, match an integer
# to a string
def matchDigit(digit):
	if digit=="1":
		return 1
	elif digit=="2":
		return 2
	elif digit=="3":
		return 3
	elif digit=="4":
		return 4
	elif digit=="5":
		return 5
	elif digit=="6":
		return 6
	elif digit=="7":
		return 7
	elif digit=="8":
		return 8
	else:
		return 9


args = sys.argv ;# careful, the first argument is the program itself

print args;

input =  list(args[1]);
print "We are converting "+ str(input)+" to int";

# Assuming no dots
# first calculate string length
# after keep moding with 1000, 100, 10 until we have all the digits

l = len(input);

output = calculateNumber(l)

print output;



