#!/usr/bin/python
# given an API for a very large array that returns either the element at index i
# or OOB exception if i bigger than length
# find upper bound of array
# execute like
#> python findUpperBound.py " 1 2"

import sys

def get_len(_list, index=1, step=1, knownLowerB=0, knownUpperB=None):
    print "*"
    # we assume list is not empty
    if knownUpperB is None:
        # we still have not fallen over
        # we increment one order of magnitude
        step = index* 2

    # end condition
    if knownLowerB+1 == knownUpperB:
        print "length {0}".format(index)
        return index

    try:
        _list[index]
        # we increase one step length every time
        knownLowerB = index
        print "Index: {0}, Step:{1}".format(index,step)
        return get_len(_list, index + step, step, knownLowerB, knownUpperB)

    except IndexError:
        print "Exceeded Length at index: {0}".format(index, knownLowerB)
        # went over the length, backtrack
        # we know one order of magnitude less works so we are now between [10, 100] or [100, 1000]
        knownUpperB = index
        nextStep = int(knownUpperB-knownLowerB)/2
        # reset index back
        nextIndex = knownLowerB + nextStep
        print "Exceeded Length at index: {0} nextIndex: {1} knownlowerB {2} nextStep: {3}".format(index, nextIndex,knownLowerB, nextStep)
        return get_len(_list, nextIndex, nextStep, knownLowerB, knownUpperB)


def main():
    inputStringList = sys.argv[1];

    # pretty basic parsing
    # TODO some error checking
    myList = inputStringList.split();

    print "Mistery list: {0}".format(myList)

    l = get_len(myList)

    print "length: {0}".format(l)

if __name__== "__main__":
    main()
