#!/usr/bin/python
# given an API for a very large array that returns either the element at index i 
# or OOB exception if i bigger than length
# find upper bound of array

class LargeArray:

	def __init__(self,largeArray):
		self.largeArray = largeArray;

	def get(self,i):
		if (i> len(self.largeArray)-1):
			# throw exception
			raise OOBException('Bad value');
		else: 
			return self.largeArray[i]


class OOBException(Exception):
	def __init__(self, value):
		self.value = value;
	def __str__(self):
		return repr(self.value)

def findUpper(la, startIndex):
	index = startIndex;
	try:
		max = la.get(startIndex);
		increment = 1;
		while(True):
			index = index+increment;
			max = la.get(index);
			increment = increment*2;
	except OOBException:
		if increment==1:
			return max
		else:
			return findUpper(la,index-increment)
	
a = [i for i in range(0,100000)]
	
la = LargeArray(a);

max = findUpper(la,0)

print(max)

