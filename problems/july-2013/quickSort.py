#!/usr/local/bin/python

# partition arround a pivot
def partition(a):
	#print(a);	
	# choose pivot is 1st element
	l = len(a);
	if (l <=1):
		return a;
	
	p =  a[0];
	#split
	k = 0;
	for i in range(0, l):
		if a[i] < p:
			#swap with pivit
			a[k] = a[i];
			a[i] = p;
			k = k+1;
		# if they are equal or a[i] > p do nothing
	
	# after this there are two groups
	a1 = partition(a[0:k]);
	a2 = partition(a[k+1:l]);

	return a1 +[p]+ a2;





a = [4,5,7,8,9,1243,56,12,23,45];

b = partition(a);
print(b);

c = [0,0,0,1,1,1,34,45];
c = partition(c);
print(c);
