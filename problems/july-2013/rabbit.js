// rabbit OO example
// to be executed on node


// when using this in global scope, it will
// refer to the global object (window in a browser)
// is empty in node?
console.log(this);

// gotcha:::::
// outside of strict mode calling a function like foo() will also refer to
// global object, instric mode will be undefined

function Foo() {
	this.hello = "hello!";
}

Foo.prototype  =  {
	
	testing :function(){

		function test(){
			console.log(this);
		}

	test();
	}
}

var foo = new Foo();

foo.testing();



//in the constructor function this is the new object
function Rabbit(name) {
	this.name = name;
}

// in method calls this is the object calling the method
Rabbit.prototype.talk = function(msg) {
	console.log(" I am "+this.name+ " ");
    console.log(msg);
}

function ScaryRabbit(name) {
   Rabbit.call(this,name);
}

// instead of having objects inherit from other objects an unnecesary level of
// indirection is inserted such that objects are produced by constructor
// functions
ScaryRabbit.prototype = new Rabbit;

ScaryRabbit.prototype.constructor = ScaryRabbit;

ScaryRabbit.prototype.scream = function(msg) {
	console.log("WWWWowww!!!");
	this.talk(msg);
}

 var r = new Rabbit('pepito');
 r.talk("hola!");

 var sr = new ScaryRabbit('juanito');
 console.log(sr);
 sr.scream(" I will eat you ");

