#!/usr/localbin/python

def mergeSort(arr):
	# first divide array in half 
	l = len(arr);
	if (l ==1):
		return arr;

	if l % 2 == 0:
		a1 = arr[0:l/2];
		a2 = arr[l/2:l];
	else: 
		n = int(l/2);
		a1 = arr[0:n];
		a2 = arr[n:l];

	# later merge both halfs
	return merge(mergeSort(a1),mergeSort(a2))

# merge two sorted arrays
def merge(a1, a2):
	# we will do it creating another array 
	# and returning that one
	s = [];
	l1 = len(a1);
	l2 = len(a2);
	j = 0;
	i = 0;
	while (i <l1 and j<l2):
		if (a1[i] <= a2[j]):
			s.append(a1[i]);
			i = i+1;
		else:
			s.append(a2[j]);
			j = j+1;

	# we finished the loop 
	# make sure we get all elements in

	if (i < l1):
		# put the rest of the i elements
		for k in range(i,l1):
			s.append(a1[k]);
	elif (j < l2):
	   # put the rest of the j elements
		for k in range(j,l2):
			s.append(a2[k]);
	
	return s;




a = [1,3,5,7,9,8]
print mergeSort(a);
