/**
 * UI for the queue system
 * **/

/**
 * Represents UI that handles one or various queue systems
 * **/
var q = (function(){
	var q = Queue(2);

	function task(){
		var d = new Date().valueOf(); // should return value in ms
		while (new Date().valueOf() < d + 1000) {
			// just wait;
		}
		console.log('completing task');

	}

	// add same task like 10 times
	for ( var i = 0; i < 10; i++) {
		q.addTask(task);
	}
	return q;
})();

/** 
 * Attaches event listeners
 * **/
function bootstrap(){
	// activating ko
	ko.applyBindings(qView);
	var startBtn = document.getElementById('start');
	startBtn.addEventListener('click', function(){

		console.log("queue started");
		q.start();

		// refresh UI
		setTimeout(function(){
			qView.refresh();
		}, 1000);

	});
}

document.addEventListener('DOMContentLoaded', bootstrap);

var qView = {

    size: ko.observable(q.size()),

    running: ko.observable(q.isRunning()),

    notRunning: ko.observable(!q.isRunning()),

    // refresh the UI according to our processing
    refresh: function(){
	    this.size(q.size());
	    this.running(q.isRunning());
	    this.notRunning(!q.isRunning());
	    if (q.isRunning()) {
		    setTimeout(function(){
			    qView.refresh();
		    }, 1000);
	    }

    }

};
