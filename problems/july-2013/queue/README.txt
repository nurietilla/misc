 Regarding the assignment I was not sure of the level of asynchronicity required.
 Thus I have made the scheduling of the task async even if the task themselves are async.
 
 I have included unit tests that only run on node written on nodeunit.
 I have tested  on my 0.8.6 version of node and they run without issues.
 It is kind of hard to test async code but nodeunit seems to handle it well.

 You can run the test after installing nodeunit as follows:
 node_modules/nodeunit/bin/nodeunit queue.js

 I have included what is a *very* meager UI
 in knockout.

 Thanks, 

 Nuria

