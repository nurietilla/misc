var node = true;
if (typeof exports != 'undefined' && exports !== null) {
	exports.Queue = Queue;
} else if (typeof window != 'undefined' && window !== null) {
	window.Queue = Queue;
	node = false;
}
/**
 * Constructs a new Queue. This function should have the same semantics 
 * whether it is called with or without the `new` keyword -- that is:
 * 
 *  var q = Queue();
 * 
 * ...Should have all the same functionality (the methods outlined below) as:
 * 
 *  var q = new Queue();
 * 
 * It is not required that the returned object be an instance of Queue.
 * 
 * @constructor
 * @param {Number} [parallelism=1] Maximum number of tasks to run in parallel;
 *  cannot be less than than 1 or an error is raised.
 * @returns {Queue} A new queue object.
 */
function Queue(parallelism){

	var q = (function(){
		// private members go here

		var running = false;
		/** in flight requests just an array **/
		var inFlight = 0;

		var tasks = [];

		/** callbacks requests to be executed once all tasks have completed **/
		var callbacks = [];

		/** tasks are executed in serie by default **/
		var parallel = parallelism ? parallelism : 1;

		/** make sure code runs on browser **/
		var _process = function(callback){

			if (node) {
				return process.nextTick(callback);
			}
			return setTimeout(callback, 200);

		};
		return {
		    /**
		     * @returns {Number} Number of tasks awaiting execution. Does not count any tasks that 
		     * are in-flight.
		     */
		    size: function(){
			    return tasks.length;
		    },

		    /**
		     * @returns {Boolean} Whether the queue is running or not.
		     */
		    isRunning: function(){
			    return running;
		    },

		    /**
		     * @returns {Number} Number of tasks currently executing.
		     */
		    inFlight: function(){
			    return inFlight;
		    },

		    /**
		     
		     * Adds a new task to the queue. When executed, the task will be invoked with the given context
		     * and passed a callback used signal the task has completed:
		     * 
		     *  task.call(context, callback)
		     * 
		     * @param {Function} task Function which begins the asynchronous task,
		     *  taking a callback to be invoked upon task completion.
		     * @param {Object} [context=this] Optional. Execution context for the task. Defaults to the queue.
		     * @returns {this} The queue object.
		     */
		    addTask: function(task, context){
			    var t;
			    if (!context) {
				    t = new Container(task, this);
			    } else {
				    t = new Container(task, context);
			    }

			    tasks.push(t);
			    return this;
		    },

		    /**
		     * Adds a callback to be invoked when all tasks have been completed:
		     * 
		     *  callback.call(context, queue)
		     * 
		     * @param {Function} callback Function to invoke when all tasks have completed;
		     *  it will be passed the queue object.
		     * @param {Object} [context=this] Execution context for the callback. Defaults to the queue.
		     * @returns {this} The queue object.
		     */
		    addCallback: function(callback, context){
			    var c;
			    if (!context) {
				    c = new Container(callback, this);
			    } else {
				    c = new Container(callback, context);
			    }
			    callbacks.push(c);
			    return this;
		    },

		    /**
		     * Begin executing queued tasks.
		     * 
		     * Queued tasks execute in order queued, but tasks do not wait for prior tasks
		     * to complete -- this implies the order of task completion is undefined. No more
		     * than `parallelism` tasks will run at once.
		     * 
		     * Queue will continue executing tasks until all have completed, at which point it
		     * executes all registered callbacks. Calling `start` repeatedly while the queue is
		     * running has no effect (though the queue can be started again with new tasks once
		     * it completes).
		     * 
		     * @returns {this} The queue object.
		     */
		    start: function(){
			    if (running) {
				    return;
			    }

			    running = true;

			    var self = this;
			    // reduces the inflight counter, task was completed
			    var completitionCallback = function(){
				    console.log("Calling completition callback of task, number of inflight:" + inFlight);
				    inFlight--;

			    };

			    /**
			     * Async scheduling of tasks
			     * @returns
			     */
			    function scheduleTask(){
				    if (this.size() > 0) {
					    console.log('Number of inflight req:' + this.inFlight());
					    if (this.inFlight() < parallel) {
						    // schedule 1 by 1 not the fastest
						    console.log('Scheduling Task');
						    var t = tasks.shift();
						    inFlight++;

						    _process(function(){
							    console.log('Exec Task');
							    t.exec(completitionCallback);
						    });
					    }

					    _process(scheduleTask.bind(self));

				    }
			    }

			    scheduleTask.bind(this)();

			    _process(wrapUp.bind(this));

			    /**
			     * Checks for task completition
			     * @returns
			     */
			    function wrapUp(){
				    console.log('Wrap Up: Number of inflight req:' + this.inFlight());

				    if (this.inFlight() > 0) {
					    // console.log('waiting for inFlight requests');
					    _process(wrapUp.bind(self));
					    return;
				    }
				    // task queue is empty, now make sure that inflight queue
				    // is also empty

				    // if there are no callbacks we are done
				    if (callbacks.length == 0) {
					    running = false;
				    }
				    function allDone(){
					    console.log("Setting running to false");
					    running = false;
				    }
				    while (callbacks.length > 0) {
					    var c = callbacks.shift();
					    c.exec(allDone);
				    }

			    }
			    return this;
		    }
		};

	})();

	return q;

}

/**
 * Logger, in real life will be posting messages via HTTP somewhere
 * In this exercise we are just keeping the log in an array 
 * for as long as our program is alive
 */
function Log(){
	this.messages = [];
}

/**
 * Adds a log message to the queue
 * @param msg
 */
Log.prototype.add = function(msg){
	this.messages.push(msg);
};

var log = new Log();

/**
 * Wrapper for a task or a callback.
 * It wraps a function and the context
 * this function executes on
 *  
 * @param fn
 * @param context
 */
function Container(fn, context){
	this.fn = fn;
	this.context = context;
}

/**
 * Executes function with given context and an optional callback
 * 
 * @param callback
 */
Container.prototype.exec = function(completitionCallback){

	try {

		this.fn.call(this.context);

	} catch (e) {
		console.log("There was an error");
		log.add("Task failure: " + this.fn.constructor.toString());
	}

	// even if task is failed we mark it as complete, we just log it failed
	if (completitionCallback) {
		completitionCallback();
	}

};

// ////////////////////// Notes ////////////////////////////////
// When it comes to errors many things could be done. I have choosen the easies approach which is
// to store in some kind of log and keep executing.
// Task execution could also be handled using promises, which incorporate already a
// semantics to deal with these

// //////////////// tests , NODEUNIT is needed for these to work ////////////////
// set up and tear down does not work that great so a little of bolierplate
// defining so the browser does not complain
if (!node) {
	module = {};
}
module.exports = {

    /**
     * Test that we can instatiate it with new
     * @param test
     */
    testInstatiation: function(test){
	    var q = new Queue();
	    test.ok(!q.isRunning());
	    test.done();
    },

    /**
     * Instantiates a queue
     * without tasks. 
     * Adds a callback, makes sure isRunning values are set correctly.
     * @param test
     */
    testHappyCase: function(test){
	    test.expect(2); // number of assertions
	    var q = Queue();
	    // ok tests that value is a true value
	    test.ok(!q.inFlight());

	    function callbackWait(){

		    // console.log('executing callback');
		    var d = new Date().valueOf(); // should return value in ms
		    while (new Date().valueOf() < d + 1000) {
			    // just wait;
		    }
		    // watch out the callback is doing the assert
		    console.log("queue is running");
		    test.ok(q.isRunning());
		    test.done();
	    }

	    q.addCallback(callbackWait, this);
	    q.start();

    },

    /**
     * Creates a task and executes it
     * @param test
     */
    testTaskExecution: function(test){
	    test.expect(2); // number of assertions
	    var q = Queue();
	    var task = function(){
		    test.ok(q.isRunning());
		    test.ok(q.inFlight() == 1);
		    test.done();
	    };

	    q.addTask(task, this);
	    q.start();

    },

    /**
     * Creates a task and executes 
     * and assets on inFlight and size counters
     * @param test
     */
    testTaskExecutionAndInFlightCounts: function(test){
	    test.expect(4); // number of assertions
	    var q = Queue();
	    var task = function(){
		    // watch out assertions are inside the task
		    test.ok(q.isRunning());
		    // size should be zero as request is in flight
		    test.equal(q.size(), 0, "size of queue should be 0");
		    test.equal(q.inFlight(), 1, "size of inflight requests should be 1");

	    };

	    q.addTask(task, this);
	    q.addCallback(function(){

		    test.equal(q.inFlight(), 0, "size of inflight requests should be 0");
		    test.done();
	    });
	    q.start();

    },

    /**
     * Create task whose execution throws an error, make sure system
     * does not stop
     * @param test
     */
    testTaskExecutionAndInFlightCountsError: function(test){
	    test.expect(3); // number of assertions
	    var q = Queue(2);
	    var task = function(){
		    // watch out assertions are inside the task
		    test.ok(q.isRunning());

		    throw {
		        name: "Terrible exception!",
		        message: "Terrible message"
		    };

	    };

	    q.addTask(task, this);

	    q.addCallback(function(){
		    test.equal(q.inFlight(), 0, "size of inflight requests should be 0,even if there was a failure");
		    test.equal(log.messages.length, 1, "There was 1 error");
		    test.done();
	    });

	    q.start();
    },

    /** Creates several tasks task and executes it
    * @param test
    */
    testTaskExecutionMany: function(test){
	    test.expect(7); // number of assertions
	    var q = Queue(2);
	    var task = function(){
		    console.log("task 1");
		    test.ok(q.isRunning());
		    test.ok(q.inFlight() == 1);

	    };
	    var task2 = function(){
		    console.log("task 2");
		    test.ok(q.isRunning());

	    };

	    var task3 = function(){
		    console.log("task 3");
		    test.ok(q.isRunning());
		    test.ok(q.inFlight() == 1);

	    };

	    q.addCallback(function(){
		    console.log("final callback");
		    test.equal(q.inFlight(), 0, "size of inflight requests should be 0,even if there was a failure");
		    test.equal(log.messages.length, 1, "There was 1 error");
		    test.done();
	    });

	    q.addTask(task, this);
	    q.addTask(task2, this);
	    q.addTask(task3, this);
	    q.start();

    },

};
