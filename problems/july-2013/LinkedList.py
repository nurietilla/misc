#!/usr/bin/python
# Reverse a linked list in place
# practice oo python as there is no formal definition of linked list
# since it is only a bout reversing we will just create the code to build a 
# linked list

class LinkedList:

	def __init__(self, root=None):
		self.root = root;
 
	def findLast(self):
		nodes = [self.root]
		while(len(nodes)!=0):
			node = nodes.pop();
			if (node.getNext() == None):
				return node;
			else:
				nodes.append(node.getNext())

	# only adds at the end
	def add(self,node):
		last = self.findLast();
		last.setNext(node);
		node.setPrior(last);
	
	def setRoot(self,node):
		self.root = node;
	
	def __str__(self):
		msg = "List Root";
		nodes = []
		if (self.root!= None):
			nodes.append(self.root)
		while(len(nodes)!= 0):
			node = nodes.pop();
			if (node == None): break
			msg = msg +"->"+ str(node); 
			nodes.append(node.getNext())
		return msg;
			


class Node:
	def __init__(self, value, next=None, prior=None):
		self.value = value;
		self.next = next;
		self.prior = prior;

	def getValue(self):
		return self.value; 
	
	def getNext(self):
		return self.next
	
	def getPrior(self):
		return self.prior

	def setNext(self,next):
		self.next = next;

	def setPrior(self,prior):
		self.prior = prior

	def __str__(self):
		return "{ value:"+str(self.getValue())+"}";#, next"+str(self.next)+",prior:"+str(self.prior)+"}"
#### 

node3 = Node(3)
node2 = Node(2)
node1 = Node(1)


ll = LinkedList(node1);

ll.add(node2);
ll.add(node3);

print str(ll);

# now reverse in place 

current = ll.findLast();
last = current;

# let's assume for simplicity that list has at list 3 elements
#beforePrior = last.getPrior().getPrior();

print current

while(current != None):
	prior = current.getPrior(); #2
	next = current.getNext(); #none
	# now change them in place 
	current.setNext(prior);#2
	current.setPrior(next);#None
	current = prior
	

ll.setRoot(last)
print ll;

