package codetest;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("data setup");
		
		BadGuy bg1 = new BadGuy(1,"pepito",100,1);
		BadGuy bg2 = new BadGuy(2,"pepito2",100,48);
		BadGuy bg3 = new BadGuy(3,"pepito3",100,48);
		BadGuy bg4 = new BadGuy(4,"pepito4",1000,240);
		BadGuy bg5 = new BadGuy(5,"pepito5",100,1);
		BadGuy bg6 = new BadGuy(6,"pepito6",100,1);
		
		List<BadGuy> informantsList1 = new ArrayList<BadGuy>();
		
		informantsList1.add(bg1);
		informantsList1.add(bg2);
		informantsList1.add(bg3);
		
		Informant informant1 = new Informant(informantsList1);
		Informant informant2 = new Informant(informantsList1);
		
		List<Informant> informantList = new ArrayList<Informant>();
		informantList.add(informant1);
		informantList.add(informant2);
		
		List<BadGuy> centralJusticeList = new ArrayList<BadGuy>();
		
		centralJusticeList.add(bg1);
		centralJusticeList.add(bg2);
		centralJusticeList.add(bg3);
		centralJusticeList.add(bg4);
		centralJusticeList.add(bg5);
		centralJusticeList.add(bg6);
		
		List<BadGuy> hunts = Scheduler.schedule(centralJusticeList, informantList);
		
		System.out.println("Done calculating unts for the week:");
		//now print list and see what we get
		Iterator<BadGuy> it = hunts.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next().toString());
		}
		
	}

}
