package codetest;

/**
 * Gets the list of the bad guys, filters by the ones I can hunt 
 * Adds those to a max heap and extracts elements from the heap until I
 * have reached the time limit.
 * 
 * We have one instance of the scheduler per badguy list
 * 
 * @author nuria
 *
 */

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.ArrayList;


public class Scheduler {

	//time limit is the number of hours in a week
	public static int timeLimit = 1*24*7;
	
	/**
	 * Given a list of BadGuys published by the central justice
	 * Returns optimal scheduling
	 * @param inputList
	 * @param informants
	 * @return
	 */
	public static List<BadGuy> schedule(List<BadGuy> inputList, List<Informant> informants){
		PriorityQueue<BadGuy> heap = Scheduler.filter(inputList,informants);
		List<BadGuy> futureHunts =  new ArrayList<BadGuy>();
		
		// now get items out of the list until
		//1) there is no more 
		// OR
		//2) we have reached our timeLimit
		
		int currentTime = 0;
		BadGuy badGuy = heap.poll();
		
		while(currentTime <= timeLimit && badGuy!=null ){
			futureHunts.add(badGuy);
			currentTime = currentTime+badGuy.getDangerPoints();
			badGuy = heap.poll();
			
		}
		
		return futureHunts;
	}
	
	
	/**
	 * 
	 * Filters the list of the bag guys by using the informants
	 * We can only hunt bag guys for which we have information
	 * @param informants
	 * @return List<BadGuy>
	 */
	private static PriorityQueue<BadGuy> filter( List<BadGuy> inputList, List<Informant> informants){
		
		System.out.println("Filtering possible hunts");
		
		// make sure we process all informants only once
		Iterator<Informant> it = informants.iterator();
		
		Set<Integer> huntable = new HashSet<Integer>();		
		
		while(it.hasNext()){
			Map<Integer,Criminal> contacts = it.next().getContacts();		
			huntable.addAll(contacts.keySet());
		}
		System.out.println("These are the ones we can hunt");
		System.out.println(huntable);
		
		// now we know which bad guys we can hunt as they are on the huntable array
		// add bad guys to the priority queue if they can be haunted
		
		PriorityQueue<BadGuy> heap = new PriorityQueue<BadGuy>();
		
		Iterator<BadGuy> badGuyIt = inputList.iterator();
		
		while(badGuyIt.hasNext()){
			BadGuy badGuy = badGuyIt.next();
			if (huntable.contains(badGuy.getCriminalId())){
				heap.add(badGuy);
			}
		}
		
		System.out.println("Heap");
		System.out.println(heap);
		return heap;
	}
	 
}
