package codetest;

/**
 * A criminal can be a badguy as defined on the bad guy list
 * or an informant
 * @author nuria
 *
 */
public interface Criminal {

	public Integer getCriminalId();
	public String getName();
}
