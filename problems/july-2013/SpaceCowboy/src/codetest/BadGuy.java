package codetest;

/**
 * Describes identifiers of a bad guy
 * 
 * Provides a means to compare bad guys
 * 
 * @author nuria
 *
 */
public class BadGuy implements Criminal,Comparable<BadGuy>{

	private Integer criminalId;
	
	private String name;
	
	private int reward;
	
	private int dangerPoints;

	public BadGuy(Integer criminalId,String name,int reward,int dangerPoints) {
		this.criminalId = criminalId;
		this.name = name;
		this.reward  = reward;
		this.dangerPoints = dangerPoints;
	}
	
	public void setCriminalId(Integer criminalId) {
		this.criminalId = criminalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getDangerPoints() {
		return dangerPoints;
	}

	public void setDangerPoints(int dangerPoints) {
		this.dangerPoints = dangerPoints;
	}

	@Override
	public  Integer getCriminalId() {
		return this.criminalId;
	}
	
	@Override
	public String toString() {
		return "ID: "+this.criminalId+ ", Reward: "+this.reward+",DangerPoints: "+this.dangerPoints;
	}

	/**
	 * Provides a compration mechanism among two bad guys
	 * A bad guy is more valuable if the ratio among 
	 * reward/danger points is bigger
	 * 
	 * Since the priority queue is a min heap this comparation will
	 * return the result times -1
	 */
	@Override
	public int compareTo(BadGuy other) {
		
		
		if ((this.reward/this.dangerPoints) > (other.reward/other.dangerPoints)){
				//this is more valuable, should be higher on min heap
				return -1;
		}if ((this.reward/this.dangerPoints) == (other.reward/other.dangerPoints)){
				return 0;
		}else {
				//this is less valuable than other, should be lower on min heap
				return 1;
		}
	}
	
	
}


