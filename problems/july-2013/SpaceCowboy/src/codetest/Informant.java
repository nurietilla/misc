package codetest;

import java.util.Map;

/**
 * An informant is your link to a set of contacts that at the 
 * end are the criminals that you want to hunt.
 * 
 * You can only hunt bad guys for which you have information
 * @author nuria
 *
 */
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;

public class Informant  {

	/** String here represents the CriminalId **/
	//we could do away with the criminal part really as we just need a list of ids
	//leaving it as it is nice to have it for printing information
	private Map<Integer,Criminal> contacts = new HashMap<Integer,Criminal>();

	/**
	 * An informat just contains a list of bad guys
	 */
	public Informant(List<BadGuy> badGuyList) {
		//process list so we can look it up by id
		Iterator<BadGuy> it = badGuyList.iterator();
		
		while(it.hasNext()){
			BadGuy badGuy = it.next();
			contacts.put(badGuy.getCriminalId(), badGuy);
		}
		
		
	}
	
	public Map<Integer, Criminal> getContacts() {
		return contacts;
	}

	
}
