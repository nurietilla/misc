#!/usr/local/bin/python
import unittest

'''Calculates the subsets of the occurrence list `List(('a', 2), ('b', 2))`:
			print size

   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
'''
# gets all combinations of length 
# the same length than the list
def combinate(possibilities,current,result):
	if len(possibilities) == 0:
		if current: # empty lists are false
			result.append(current);
	
	else:
		# does this work?
		# get all items of the same flavour
		identifier = possibilities[0][0];
		flavour = [i for i in possibilities if i[0] == identifier];
		remaining = [i for i in possibilities if i[0]!= identifier];
		
		print "flavour"+str(flavour);
		print "remaining"+str(remaining);

		for f in flavour:
			_current = list(current);
			_current.append(f);
			result.append([f]);
			
			print "appending "+str(f)+" to result";

			combinate(remaining,_current,result)


	


_arr = [('a',2),('b',2)];

maxSetSize = len(_arr);

# first we need to calculate the possibilities list

originalPossibilities = [];

for item in _arr:
	for i in range(1,item[1]+1):
		originalPossibilities.append((item[0],i));
	 

print originalPossibilities;
current =[];
result =[]

combinate(originalPossibilities,current,result);
print result;


