#!/usr/bin/python
# Write a function that will display all possible arrangements of letters in a
# string. Example: abc acb bac bca cab cba

import sys




def  permutate(things):
	permutations = []
	if (len(things) == 1):
		return [things]
	else:
		for item in things:
			# remove removes 1st ocurrence of item
			remaining = list(things);
			#print remaining
			remaining.remove(item);
			_permutations = permutate(remaining);
			#print "inner permutations"+str(_permutations)
			# for each element in permutations add our "header" element
			# at the beginning
			for p in _permutations:
				#print p
				p.append(item);
				p.reverse();
				permutations.append(p);
	

	return permutations;

args = sys.argv #careful first argument is the program itself

print " Finding all permutations of "+ args[1];

items = list(args[1]);


permutations = permutate(items);

for item in permutations:
	print "".join(item)
