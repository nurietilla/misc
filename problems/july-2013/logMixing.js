// this can be added to any obj with a Logger.call(this) call
// mixings must know much about the objects they are augmenting 
//
function Logger(){
	this.log = function(msg){
		console.log("LOG "+msg);
	}
}

function Rabbit(name){
	this.name = name;
	Logger.call(this)
}

var r = new Rabbit("Roger");
r.log("hey I am logging although I am a rabbit");
