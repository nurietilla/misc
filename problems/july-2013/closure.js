
// good closure example from our talk
function setUpCounter(startValue){
	var num = startValue;

	// these three functions below are going to exists in global scope
	getNumber = function() { return num}
	setNumber = function(x) { num = x }
	decreaseNumber = function() {return num-1}

}

setUpCounter(10);
console.log(decreaseNumber());
console.log(setNumber(100));
console.log(decreaseNumber());
