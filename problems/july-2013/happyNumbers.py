#!/usr/bin/python
# Write pseudo code that take as input a natural number C ,
# and outputs all of the ways that a group of ascending positive numbers can
# be summed to give C . For example, if C = 6, the output should be
# 1+2+3
# 1+5
# 2+4
# 6
# and if C = 10, the output should be
# 1+2+3+4
# 1+2+7
# 1+3+6
# 1+4+5
# 1+9
# 2+3+5
# 2+8
# 3+7
# 4+6
# 10


import sys;


# given a number 'n' returns a list
# of ascending postive numbers that n can be decomposed on
# it will calculate duplicates like [1,2,3] and [3,2,1] if input is 6
# but those get removed before being part of the solution
def decompose(n):
	# l is a list of lists
	l = [];
	l.append([n]);
	if n>2:
		if n % 2 == 0:
			m = n/2
		else:
			m = int(n/2)+1;

		for i in range(1,m):
			subList = decompose(n-i);
			# for each sublist append 'i'
			validSolutions =[]
			for element in subList:
				if i not in element:
					element.append(i)
					_sortedList = sorted(element);
					if _sortedList not in l:
						validSolutions.append(_sortedList)
		
			# append to our main list
			l.extend(validSolutions);

	return l


argList = sys.argv; # careful first number is the program itself

target = int(argList[1]);

print " We will be looking to decompose" +str(target)

solution = decompose(target);

# print output
for element in solution:
	print element;

