
function Foo(){
	console.log('creating obj');
	this.name ='pepe'; //refers to global object but within the context of 'new'
			   //refers to newly created object
}

Foo.prototype = {

	inner: function() {
		console.log(this);
		function inner2() {
			console.log(this);
			console.log(this.name);
		}
		inner2();
	}

}

var foo = new Foo();

foo.inner();
