#!/usr/local/bin/python

import sys
import datetime
import shutil
from datetime import timedelta
from datetime import date
import os

# simplified version of analytics interviewing task:
# https://gist.github.com/ottomata/d946fcdaae03340eb102
# For now call like: python delete-directories.py 8 ./data True

# walks directory tree and deletes if proceeds
# if dryrun just prints what is going to be deleted
# three levels 0 -root 1 -year 2 -month 3 -day
def walk_tree(node, level, year, month, day, dryRun):
    children = os.listdir(node)
    for c in children:
        child = os.path.join(node, c)
        # convert to integer
        if os.path.isdir(child):
            if (level == 1 and int(c) < year) or (level ==2 and int(c) < month) or (level == 3 and int(c) < day):
                print child
                if not dryRun:
                    shutil.rmtree(child, True)
            else:
                walk_tree(child, level+1, year, month, day, dryRun)


def main():
    args = sys.argv
    #TODO: nice formatted arguments
    inputDays = int(args[1])
    rootPath = args[2]
    dryRun = bool(args[3])
    print "Will drop data older than {0} days".format(inputDays)
    delta = timedelta(days=inputDays)
    print "we need to substract {0} days".format(delta.days)
    today = date.today();
    print "and today is {0}".format(today)

    fromDate = today - delta
    print "deleting data from {0}".format(fromDate)
    print "oldest year {0}".format(fromDate.year)
    print "oldest month {0}".format(fromDate.month)
    print "oldest day {0}".format(fromDate.day)

    oldestYear = fromDate.year;
    oldestMonth = fromDate.month;
    oldestDay = fromDate.day;

    walk_tree(rootPath, 0, oldestYear, oldestMonth, oldestDay, dryRun )






if __name__=="__main__":
    main()


