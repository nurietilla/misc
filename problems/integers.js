//Given an array of integers, return all integers in a particular range.
//




var _a = [9,23,45,2,3,4,56,567,234,789,1000,1001,1002,123457,32,21,98,1];

function findInRange(_a,lb,ub){
	var i = 0;
	var l =_a.length;
	
	for (i =0;i<l;i++) {
		if (_a[i] >lb && _a[i]<ub){
			console.log(_a[i]);
		}

	}

}

//findInRange(_a,7,35);

/**Given an array of integers 
 * where some numbers repeat 1 time, 
 * some numbers repeat 2 times and only one 
 * number repeats 3 times, how do you find the 
 * number that repeat 3 times. 
 * Using hash was not allowed. Complexity of algorithm should be O(n)
 */


var _a = [9,23,45,2,3,3,4,4,56,567,234,789,1000,1001,1002,1002,123457,32,21,98,1,1,1];

function find3TimesRepeat(_a){
	var i,l,_temp;
	i=0;
	l=_a.length;
	_temp = new Array();

	for (i=0;i<l;i++) {
		var item = _a[i];
		var tmp = _temp[item];

		if (tmp) {
			_temp[item] = tmp+1;
		} else {
		   _temp[item] = 1;
		}

	}
	
	var j=0,len=_temp.length;
	for (j=0;j<len;j++) {
		if(_temp[j]==3) {
			console.log(j);
		}
	}


}


find3TimesRepeat(_a);
