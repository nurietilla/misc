## Iterative and recursive fibonacci


def rFib(i):
	#print "Recursive i="+str(i) 
	if (i ==1 or i==0):
		return 1;
	else:
		return rFib(i-2)+rFib(i-1)


def iFib(i):
	print "Iterative i="+str(i)
	if i == 0 or i==1:
		return 1;

	minusOne = 1;
	minusTwo = 1;
	fib = 0
	for n in range(1,i):
		fib = minusOne+minusTwo
		minusTwo = minusOne
		minusOne = fib
	return fib


print iFib(1)
print rFib(1)
print iFib(2)
print rFib(2)
print iFib(3)
print iFib(7)
print rFib(7)
