/**
 * [2, 3, 5, 4, 8, 7, 6]
 * **/

var a = [1,2, 3, 5, 4, 8, 7];

function bootStrap(_a) {

	var a = _a;
	var root = a[0];

	
	function setChildren(node,i){
		
		console.log(node);
	
		if (a[2*i+1]){
			var n = a[2*i+1];
			console.log("right node of"+node+"is"+n);
			
            setChildren(n,2*i+1);
		} 

		if (a[2*i+2]) {
			var n = a[2*i+2];
			console.log("left node of"+node+"is"+n);
			setChildren(n,2*i+2);
		}
		//do nothing if there are no more children
	}
	setChildren(root,0);
}

bootStrap(a);

