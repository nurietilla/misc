/**
 * All permutations of a string 'abcd'
 */

var _input='acbd';

var _chars = _input.split("");

_chars.sort();





function permute(_charList) {
	var _length = _charList.length;
	var used = new Array();
	var out="";

	doPermute(_charList,out,used,_length,0);

}

function doPermute(_charList,out,used,_length,_level) {
	if(_level==_length){ 
		console.log(out);
		return;
	}
	var i=0;
	for (i=0;i<_length;i++) {
		if (!(used[i])){
			console.log("====");
			out = out+_charList[i];
			used[i]= true;
			doPermute(_charList,out,used,_length,_level+1);
			used[i] = false;
			console.log(i);
			console.log(_charList[i]);
			console.log(out);
			console.log("====");
//			out = out.substring(0,out.length-1);
		}

	}
}

permute(_chars);
