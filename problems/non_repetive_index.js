/**
 * given an integer array return the index of the first non
 * repetive integer
 * */

function findNonRepetive(a) {
	//create expression with xor
	var i,len=a.length;
	var expression ='' ; //make it as text, we will eval it later.
	for (i=0;i<len-1;i++) {
		expression = expression+" "+a[i].toString(2)+" ^ ";
	}

	expression = expression+" "+ a[len-2].toString(2);
	var result = eval(expression);
//	console.log(expression);
//	console.log(result);

	
	return parseInt(result,2);


}

var a =[123,3,4,5,5,4,3,2,2,67,67,98,98];
result = findNonRepetive(a);
console.log(result);
