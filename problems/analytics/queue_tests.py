import unittest
from queue import Queue, OutOfBounds, Task
from nose.tools import assert_true, assert_equal, assert_false, raises


class QueueTest(unittest.TestCase):

    def setUp(self):
        self.q = Queue(2)

    def tearDown(self):
        pass

    @raises(OutOfBounds)
    def testInit(self):
        Queue(-1)

    def testaddTask(self):
        # not sure how tp test concurrency other
        # than modifying global state....
        # looks like nose runs these asserts
        self.counter = 10
        import time

        def decrementCounter():
            time.sleep(5)
            assert_true(self.q.isRunning())
            assert_true(self.q.size() == 2)
            self.counter = self.counter-1
            print "The counter is:{0} ".format(self.counter)
            assert_equal(self.counter, 9)

        def decrementCounterFurther():
            time.sleep(5)
            import threading
            assert_true(threading.activeCount()==2)
            self.counter = self.counter-1
            print "The counter is:{0} ".format(self.counter)
            assert_equal(self.counter, 8)

        def decrementCounterAgain():
            self.counter = self.counter-1
            print "The counter is:{0} ".format(self.counter)
            assert_equal(self.counter, 7)

        t  = Task(decrementCounter, 1)
        t2 = Task(decrementCounterFurther,2)
        t3 = Task(decrementCounterAgain,3)
        self.q.addTask(t)
        self.q.addTask(t2)
        self.q.addTask(t3)
        assert_false(self.q.isRunning())
        self.q.start()

    def testTaskCallback(self):
        def returnThree():
            return 3
        t = Task(returnThree, 1)
        result = t._finish()
        assert_true(result, 3)
