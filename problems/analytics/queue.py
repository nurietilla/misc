#!/usr/local/bin/python
import threading


class OutOfBounds(Exception):
    pass


class Queue:

    def __init__(self, parallelism=1):
        '''
        No restrictions on instantiation
        Q : restrict instantiation
        '''
        if parallelism < 1:
            raise OutOfBounds("parameter out of bounds")

        self.poolSize = parallelism

        # choice of datastructure to store tasks matters
        # taking perf penalty using an array, could also use deque
        self.tasks = []

        # we assume tasks are identified by different ids
        self.inFlight = set()
        self.callback = None

    def addTask(self, t):
        self.tasks.append(t)

    def start(self):
        # since we have no event loop trigger task execution from here
        if not self.isRunning():
            self._dispatch()

    def isRunning(self):
        '''
        Returns true if queue has more than one task in flight
        '''
        return len(self.inFlight) > 0

    def inFlight(self):
        return len(self.inFlight)

    def size(self):
        return len(self.tasks)

    def addCallback(self, callback):
        self.callback = callback

    def _dispatch(self):
        '''
        Execute one of the pending tasks
        If there are no tasks it executes the finish method
        '''
        def run(task, q):
            '''
            Mean to be executed on a thread
            '''
            # Q: communicate success/failure of task
            task.execute()
            q.inFlight.remove(task_id)
            # run another task
            q._dispatch()

        if len(self.tasks) == 0 and len(self.inFlight) == 0 and self.callback is not None:
            self.finish()
        else:
            # can we execute a new task?
            if len(self.tasks) >0 and  len(self.inFlight) < self.poolSize:
                print "getting another thread from the pool"
                print "tasks"
                print len(self.tasks)
                print "in flight"
                print len(self.inFlight)
                t = self.tasks.pop(0)
                task_id = t.getIdentifier()
                self.inFlight.add(task_id)
                thread = threading.Thread(target=run, args=(t, self))
                thread.start()

    def _finish(self):
        '''
        Executes callback, callback is executed once
        Q: many executions of callback upon completition?
        '''
        if self.callback is not None:
            self.callback()
            self.callBack = None

    def __str__(self):
        return " Queue: parallelism:{0} ".format(self.p)


class Task:

    def __init__(self, callback, identifier):
        self.callback = callback
        self.identifier = identifier

    def getIdentifier(self):
        return self.identifier

    def execute(self):
        '''
        Pretend execute method for task
        '''
        print "Executing task id: {0}".format(self.identifier)
        return self._finish()

    def _finish(self):
        '''
         finishing the task executes the callback
         and returns its result
         kind of easy cause there are no scopes here
        '''
        return self.callback()


def initialize():
    print " Initializing does not do anything ..."


if __name__ == '__main__':
    initialize()
