#!/usr/local/lib
import sys;

# first argument is haystack, second is needle
# do it, do it well, do it better

haystack = sys.argv[1];
needle = sys.argv[2];

print "looking for needle({0}) on haystack {1} ".format(needle, haystack)

len_h = len(haystack)
len_n = len(needle)

for i in range(len_h):
    for n in range(len_n):
        # see if there is a match with beginning of needle
        if haystack[i+n] == needle[n]:
            # there is a match, keep on matching
            match = True
        else:
            match = False
        if not match:
            break
        elif n == len_n-1:
            # we are done, exit
            break;

    if match:
        print "Needle occurs in haystack at index {0} ".format(i)
        break

