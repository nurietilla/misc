//assume that we have the variable
//countryCodes which is equal to the array of country codes
//like:
//var countries = ["United States","Canada"];
countries.sort();

//attach event on keypress on input
//
//everytime a character is matched try to match it with the list
//do display an overlay with matches



function autocomplete(e) {
		
		var code = (e.keyCode ? e.keyCode : e.which);

		var city = document.getElementById("city");
		var q = city.value.trim();

		var i,l=countries.length;

		var matches = new Array();

		var ql = q.length; 

		for (i=0;i<l;i++) {
				//find countries that match
				var country = countries[i].slice(0,ql);
				//get the first length characters
				if(country.toLowerCase()==q.toLowerCase()){
						matches.push(countries[i]);
				}

		}

		var j , len = matches.length,choices="";

    var overlay = document.getElementById("overlay");
		//hide if no match
		if(len==0) {
				 overlay.style['display'] = 'none';
				 return;
		}
		
		overlay.innerHTML = buildListWithChoices(matches);
		//now position box right under input
		var r = city.getBoundingClientRect();
		var _left = r.left;
		var _top =r.top;


		overlay.style['display'] = 'block';
		overlay.style['left'] = _left;
		overlay.style['top']= _top+20;


}


function buildListWithChoices(choices) {
		var txt = "<ul id=\"choices\">";
    var len = choices.length;

		for (j=0;j<len;j++) {
				txt = txt +"<li class=\"choice\">"+choices[j]+"</li>";

		}

		txt = txt+"</ul>";
		return txt;
}


function swapChoice(ev) {
		var target = ev.target;
		if (target.className ==="choice") {
       //swap value on box
			 document.getElementById("city").value = target.innerText;
		}


}

window.onload = function (){
		//add keyup event
		var city = document.getElementById("city");
		city.addEventListener('keyup',autocomplete);

		var overlay = document.getElementById("container");
		overlay.addEventListener('click',swapChoice);

}
