/** Reverse a linked list in place **/

/**
 * For a singly linked in list
 * to reverse it in place you need to keep track of three pointers
 */
function reverse(list){
	//assuming there is at least four elements.
	var current = list.getFirst();
	
	var after = current.getNext();

	var twoAfter = after.getNext();

	list.setLast(list.getFirst());

    current.setNext(null);	

	while(twoAfter!==null){
		after.setNext(current); //send pointer back
		current = after;
		after = twoAfter;
		twoAfter = twoAfter.getNext();
	}
	//flip the last bit
	after.setNext(current); 

	list.setFirst(after);

	return list;
}

function Item(name) {
	
  this.name = name;
  this.next = null;
}

Item.prototype = {
	getName: function() {
		return this.name;
	},
	setNext: function(nextItem){
		this.next = nextItem;
	},

	getNext: function(){
		return this.next;
	}

}

function List(firstItem) {
	this.last = firstItem;
	this.first = firstItem;

}

List.prototype = {

	setLast:function(item){
		this.last = item;
	},
	setFirst:function(item){
		this.first = item;
	},

	getFirst: function(){
		return this.first;
	},

	getLast:function(){
		return this.last;
	},
	add: function(item){
		this.last.setNext(item);
		this.last = item;
	},

	serialize: function(){
		var current = this.first;
		var msg = "{";

		do{
			msg = msg+ " "+current.name+" ";
			current = current.next;
		} while(current !=null);
		msg = msg + "}";

		return msg;
	
	}

}

var i1 = new Item('1');
var i2 = new Item('2');
var i3 = new Item('3');
var i4 = new Item('4');



var l = new List(i1);
l.add(i2);
l.add(i3);
l.add(i4);

console.log(l.serialize());

var r = reverse(l);
console.log(r.serialize());
