#!/usr/local/bin/python
# Write a method to return all permutations of a string.

# let's cache some operations
cache = {}


def get_permutations(text_list, possibilities):
    key = ''.join(text_list)
    if cache.get(key) is not None:
        return cache.get(key)
    # text_list is an array of chars
    # if text is of length 1 just return text
    l = len(text_list)
    if l == 1:
        return text_list
    elif l == 2:
        # return the two possible permutations
        # as a list of two elements
        revert = [text_list[1], text_list[0]]
        return [text_list, revert]
    else:
        # this loop for text = abc
        # does:
        # a + permutations(bc)
        # b + permutations(ca)
        # c + permutations(ab)
        tmp_list = []
        index = 0
        for item in text_list:
            text_to_permutate = text_list[index+1:] + tmp_list
            results = get_permutations(text_to_permutate, [])
            index = index + 1
            tmp_list.append(item)
            for r in results:
                possibilities.append([item]+r)

        cache[key] = possibilities
        return possibilities


def main():
    import sys
    t = list(sys.argv[1])
    permutations = get_permutations(t, [])
    for p in permutations:
        print ''.join(p)


if __name__ == "__main__":
    main()
