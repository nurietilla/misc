
/***
 * Fast two class inheritance in javscript
 * **/

function Rabbit(name) {
	this.name = name;
	
}

Rabbit.prototype.speak = function(msg) {
console.log("rabbit says:"+msg);

}

function KillerRabbit(eyeColor,name) {
    Rabbit.call(name);
	this.eyeColor = eyeColor;
}

KillerRabbit.prototype  = new Rabbit;

KillerRabbit.prototype.kill = function () {
	console.log("i am going to kill yuuu");
}

KillerRabbit.prototype.constructor = KillerRabbit;

var rabbit = new Rabbit();
rabbit.speak('hola');

var killer = new KillerRabbit('red');
killer.kill();
//
console.log(killer);

