/**
 * You have an array with strings and numbers. Some of the strings are repeated.
 * We want a function that returns the three most repeated strings and how many
 * times they appeared in the array. Also, remove any repeated string from the
 * array, leaving only the first instance of each one. Don’t change the numbers
 * in the array nor his relative position. Example:
 *
 * Original array [ ‘a’, ‘b’, 3, ‘a’, 2, ‘c’, ‘c’, ‘b’, 3, ‘d’, ‘a’, ‘a’, 1,
 * ‘e’, ‘e’, ‘b’ ]
 * Output : ‘a (4 times), b (3 times), c (2 times)’
 * Resulting array [ ‘a’, ‘b’, 3, 2, ‘c’, 3, ‘d’,1, ‘e’ ]
 * **/

//should be fast, have done this one before
//

var a = ['a','b','3','a','2','c','c','b','3','d','a','a','1','9','e','e','b'];


var repeats = {};
function filterRepeats(element) {
	if(!repeats[element]) {
		repeats[element] = true;
		return true;
	}

	return false;
}



a = a.filter(filterRepeats);
console.log(a);
