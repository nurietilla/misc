
function Rabbit(name) {
	this.name = name;

}

Rabbit.prototype = {

	talk: function(msg) {
		console.log(" Rabbit "+this.name+"says: "+msg);
		function test () {
			console.log(this);

		}
		test();
	}

}

function KillerRabbit(name){
	Rabbit.call(this, name);
	
}

KillerRabbit.prototype = Rabbit;
KillerRabbit.constructor = KillerRabbit;

KillerRabbit.prototype.talk = function(msg){

	console.log("killer rabbit called "+this.name+" says: "+msg);

}

var r = new Rabbit('juanito');
r.talk('love yaaa');

var kr = new KillerRabbit('pepito');
kr.talk("holaaa");
