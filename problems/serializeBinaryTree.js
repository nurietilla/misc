function Tree(node) {
	this.root = node;
}

Tree.prototype = {

	serialize: function(){
      var txt = '';
	  var nodes = new Array();
	  nodes[0] = this.root;

	  while(nodes.length>0) {
			var tmpNodes = new Array();		
		   
			for (var i=0;i<nodes.length;i++) {
				node = nodes[i];
				if (node==="E") {
					txt = txt+"E,";
					continue;
				}
				
				txt = txt+node+",";

				if (node.getLeft()!=null){
					tmpNodes.push(node.getLeft());
					
				} else {
					tmpNodes.push("E");
				}

				if(node.getRight()!=null) {
					tmpNodes.push(node.getRight());
				} else {
				  tmpNodes.push("E");
				}

			}
			
			nodes = tmpNodes;
			//remove last comma
			txt = txt.slice(0,txt.length-1);
			txt = txt+"!";

	   } //end while

	  //remove las '!";
	  txt = txt.slice(0,txt.length-1);
	  return txt;	
	}, //end serialize
	
	bootstrap: function(_string) {
		//split the string by levels

		var levels = _string.split("!");
		console.log(levels);

		//instantiate root node
		var root = new Node(levels[0]);
		this.root = root;
		levels.shift();//remove top level

		//initialize nodes array
		var nodes = new Array();
		nodes[0] = root;

		while(nodes.length>0) {
			
			var level = levels.shift(); 
			var items = level.split(",");
			
			
			var len = items.length;
			var j = 0;
			var tmpNodes = new Array();

			for (var i=0;i<len;i++) {
				node = nodes[j];

				if (items[i]!='E'){
					var n = new Node(items[i]);
					node.setLeft(n);
					tmpNodes.push(n);
				}

				if(items[i+1]!='E'){
					var n = new Node(items[i+1]);
					node.setRight(n);
					tmpNodes.push(n);
				}
				console.log("node");
				i++;
				j++;
			}


			nodes = tmpNodes;
			
		}
	}


}

function Node(value) {
	this.value = value;
	this.right = null;
	this.left = null;
}

Node.prototype = {

	getRight: function() {
		return this.right;
	},
	setRight:function(node){
		this.right = node;
	},
	setLeft: function(node){
		this.left = node;
	},
	getLeft: function(){
		return this.left;
	},

	toString: function(){
		return this.value;
	}
}

var n1 = new Node(1);
var n2 = new Node(2);
var n3 = new Node(3);
var n7 = new Node(7);

var n8 = new Node(8);
var n6 = new Node(6);
var root =n3;
n2.setLeft(n1);
n3.setLeft(n2);
n3.setRight(n7);
n7.setRight(n8);
n7.setLeft(n6);

var n5 = new Node(5);
n6.setLeft(n5);

var n11 = new Node(11);
n8.setRight(n11);

var t = new Tree(root);
console.log(t.serialize());


//now how do you read it
var seriazlized = t.serialize();


var t2 = new Tree();

t2.bootstrap(seriazlized);

console.log(t2.serialize());
