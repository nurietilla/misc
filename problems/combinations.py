import copy 
import unittest

'''Calculates the subsets of the occurrence list `List(('a', 2), ('b', 2))`:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
'''

# fBuckets is forwardBuckets
# returns a list of combinations
def combinate(fBuckets):
	print "bucket"
	print fBuckets
	results = []
	# base case
	if len(fBuckets)==1:
		# outcome is just every element
		# of the bucket  as a list, i.e a list of lists 
		elements = items[fBuckets[0]]
		for e in elements:
			results.append([e])
		print "len 1 bucket results"+str(results)
		return results
	# general case
	else:
		_fBuckets = fBuckets[1:]
		elements = items[fBuckets[0]]
		print "_fbuckets"
		print _fBuckets
		# combinations w/o elements from that bucket
		for e in elements:
			_results = merge(e,combinate(_fBuckets))
			print "element "+str(e)
			print "_results  after merge "+str(_results)
			# _results is a list of lists, add it to results element by element
			for r in _results:
				results.append(r[:])
		# now add combinations minus the bucket
		for c in combinate(_fBuckets):
			results.append(c)
		return results;

 

# merges (a,1) with a list like ([(b,1)],[(b,2)]]
# returning [[(a,1),(b,1)], [(a,1),(b,2)]]
def merge(item, listOfPossibilities):
	results = [] #list of lists
	results.append([item])
	for l in listOfPossibilities:
		#possibility plus item
		l.append(item)
		results.append(l)
	return results



l = (('a',2), ('b',2))
items = {}
singleItems = []
for t in l:
	p = []
	for i in range(1,t[1]+1):
		p.append((t[0],i))
	items[t[0]] = p

print(items)

buckets = items.keys(); #[a,b]

print buckets 

combinations = combinate(buckets)

for c in combinations:
	print "\n";
	print c




