/**You have an array with strings and numbers. 
 *
 * Some of the strings are repeated. We want a function that returns 
 * the three most repeated strings and how many times they appeared 
 * in the array. Also, remove any repeated string from the array, 
 * leaving only the first instance of each one. Don’t change the numbers in the array nor his relative position. Example:
**/
/**Original array [ ‘a’, ‘b’, 3, ‘a’, 2, ‘c’, ‘c’, ‘b’, 3, ‘d’, ‘a’, ‘a’, 1, ‘e’, ‘e’, ‘b’ ]
Output : ‘a (4 times), b (3 times), c (2 times)’
Resulting array [ ‘a’, ‘b’, 3, 2, ‘c’, 3, ‘d’,1, ‘e’ ]
**/

var a =[ "a", "b", 3, "a", 2, "c", "c", "b", 3, "d", "a", "a", 1, "e", "e", "b" ];

function reportRepeated(a) {

	//need to iterate over the array
	
	var i, len = a.length;
	var current = null ;
	var currentRep = 0;
	var distinct = new Array();
	var track = {};
	var repetitions = new Array();

	for (i=0;i<len;i++) {
		var item = a[i];
		if (!track[item]) {
			track[item]=1;
			distinct.push(item);
		}

	}

	console.log(distinct);

	a.sort();

	for (i=0;i<len;i++) {
		var item = a[i];
			
		
		if (typeof item !="string") {
			continue;
		}

		if (current===null){
			current = item;
		}

		if(current===item){
			currentRep = currentRep +1;
		} else {
			//store repetitions
			repetitions.push({'item':item, 'rep':currentRep });
			current = item;
			currentRep = 1;

		}
	}
	
	//now sort repetitions array
	//
	function printRep(obj) {
		return obj.item + " "+ obj.rep+" times";
	}
	
	repetitions.sort(function(a,b){ return b.rep-a.rep})
	console.log(printRep(repetitions[0]));
	console.log(printRep(repetitions[1]));
	console.log(printRep(repetitions[2]));
	
}

reportRepeated(a);
